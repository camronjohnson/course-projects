// Main Class: 		 CompanyHierarchyMain.java
// File:		     CompanyHierarchy.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

/** The CompanyHierarchy class maintains the tree
 * 
 *  @author Camron Johnson
 */

import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CompanyHierarchy {
	private TreeNode root;
	//Used for recursive search of node
	private TreeNode nodeForSearching;
	//Used for recursive search of lists of nodes
	private List<Employee> nodeList;
	//Used for recursive search to find level of person
	private int levelOfEmployee;
	private int numOfPeople;
	private String wrongNameMessage = "Incorrect employee name for id!";

        /** Constructs a CompanyHierarchy tree  */
	public CompanyHierarchy(){
		root = null;
		numOfPeople = 0;
	}

	/** Get the name of the CEO in this company tree */	
	public String getCEO(){
		if (root == null) {
			return null;
		}
		else {
			return root.getEmployee().getName();
		}
	}
	
	/** Return the number of employees in this company tree*/
	public int getNumEmployees() {
		if (root == null) { // null check
			return 0;
		}
		else {
			return numOfPeople;
		}
	}
	
	/** Return the number of levels in the tree : 0+ values*/
	public int getMaxLevels() {
		return getMaxLevels(root); // call helper method
	}
	/** Returns the maximum height by moving down a TreeNode path with the most 
 		* nodes; otherwise returns 0 if input parameter node is null or 1 if root  
 		* is the only node in the tree */
	private int getMaxLevels(TreeNode n) {
		if (n == null) { // null check
			return 0;
		}
		if (n.getWorkers().isEmpty()) { // return 1 if root is only node in tree
			return 1;
		}
		int maxHt = 0;
		// get iterator for list of workers
		Iterator<TreeNode> itr = n.getWorkers().iterator();
		while (itr.hasNext()) {
			int workersHt = getMaxLevels(itr.next());
			// reassign maxHt if workersHt is larger
			if (workersHt > maxHt) maxHt = workersHt;
		}
		return 1 + maxHt;
	}

	/** Return the employee details of given employee id and name; return null 
        * if no such employee was found */
	public Employee getEmployee(int id, String name) {
		TreeNode returnNode = getEmployee(root, id, name); // call private helper method
		if (returnNode == null) {
			return null; // return null if no employee found
		}
		return returnNode.getEmployee();
	}
	/** Return the employee node for the corresponding employee details; return null 
     	* if no such employee was found */
	private TreeNode getEmployee(TreeNode n, int id, String name) {
		if (n == null) {
			return null;
		}
		if (n.getEmployee().getId() == id) { // check id
			if (nameCheck(n.getEmployee(), name, wrongNameMessage)) {
				return n;
			}
		}
		Iterator<TreeNode> itr = n.getWorkers().iterator(); // get iterator for list of workers
		nodeForSearching = null;
		while (itr.hasNext()) {
				nodeForSearching = getEmployee(itr.next(), id, name);
				if (nodeForSearching != null) { // if node is found, return it
					return nodeForSearching;
				}
		}
		return nodeForSearching;
	}
	/** Returns true if employee's name  matches the user input name; 
     	* returns false if no such employee exists or throw an exception  
     	* if employee exists in the Company Hierarchy tree but the name does not match */
	private boolean nameCheck(Employee employee, String name, String message) {
		if (employee == null) { // null check
			return false;
		}
		else if (employee.getName().equals(name)) { // return true if name matches
			return true;
		}
		else {
			throw new CompanyHierarchyException(message);
		}
	}
	
	/** Adds employee as a child to the given supervisor node if supervisor 
        * exists on tree; adds employee as root node if root node is null */
	public boolean addEmployee(Employee employee, int supervisorId, 
                                    String supervisorName) {
		// if tree is empty, add new employee at root
		if (root == null && supervisorName == null) {
			root = new TreeNode(employee, null);
			numOfPeople++;
			return true;
		}
		try {
			// checks if new employee is already in tree
			if (getEmployee(employee.getId(), employee.getName()) != null) {
				return false;
			}
		}
		catch (CompanyHierarchyException e) {
			// check to see if Id is already used
			if(checkId(root, employee.getId())) {
				throw new CompanyHierarchyException("Id already used!");
			}
		}
		try {
			// checks if supervisor is in tree
			TreeNode supervisorNode = getEmployee(root, supervisorId, supervisorName);
			if (supervisorNode == null) {
				return false;
			}
			// if new employee is not already in tree and their supervisor is in tree, add new employee
			else { 
				supervisorNode.addWorker(new TreeNode(employee, supervisorNode));
				numOfPeople++;
				return true;
			}
		}
		catch (CompanyHierarchyException e) {
			// if supervisor is not in tree, throw exception
			throw new CompanyHierarchyException("Incorrect supervisor name for id!");
		}
	}
	/** Returns true if a node's (or employee's) id detail matches the input id;
	 	* returns false otherwise */
	private boolean checkId(TreeNode n, int id) {
		if (n == null) { // null check
			return false;
		}
		if (n.getEmployee().getId() == id) { // check id
			return true;
		}
		Iterator<TreeNode> itr = n.getWorkers().iterator(); // get iterator for list of workers
		boolean match = false;
		while (itr.hasNext()) {
			match = checkId(itr.next(), id);
			if (match) {
				return match;
			}
		}
		return match;
	}

	/** Returns true/false based on whether the given employee exists on the 
        * tree */
	public boolean contains(int id, String name, String exceptionMessage) {
		Employee employee = getEmployee(id, name);
		if (employee != null) { // return true if employee is found
			return true;
		}
		nameCheck(employee, name, exceptionMessage); // if employee is not found, call nameCheck method
		return false;
	}

	
	/** Removes the given employee(if found on the tree) and updates all the 
 	* workers to report to the given employee's supervisor; Returns true or 
        * false accordingly */	
	public boolean removeEmployee(int id, String name) {
		TreeNode employee = getEmployee(root, id, name);
		if (employee == null) { // checks if employee is in tree
			return false;
		}
		if (employee.equals(root)) {
			throw new CompanyHierarchyException("Cannot remove CEO of the company!");
		}
		if (employee.getWorkers().isEmpty()) { //case when employee is leaf
			List<TreeNode> list = employee.getSupervisor().getWorkers();
			//remove employee from supervisor's list of workers
			for (int i = 0; i < list.size(); i ++) {
				if (list.get(i).equals(employee)) {
					list.remove(i);
				}
			}
		}
		else{ //case when employee is not leaf
			List<TreeNode> workersList = employee.getWorkers();
			// update supervisor for each worker of the old employee
			for (int i = 0; i < workersList.size(); i++) {
				workersList.get(i).updateSupervisor(employee.getSupervisor());
			}
			List<TreeNode> list = employee.getSupervisor().getWorkers();
			//remove employee from supervisor's list of workers
			for (int i = 0; i < employee.getSupervisor().getWorkers().size(); i++) {
				if (list.get(i).equals(employee)) {
					list.remove(i);
				}
			}
			// add workers of removed employee to removed employee's supervisor's list of workers
			for (int i = 0; i < workersList.size(); i++) {
				list.add(workersList.get(i));
			}
		}
		numOfPeople--;
		return true;
	}

	/** Replaces the given employee(if found on the tree) and if title of old
        *  and new employee match; Returns true or false accordingly */
    public boolean replaceEmployee(int id, String name, Employee newEmployee) {
    	if(checkId(root, newEmployee.getId())) { // check if id is in tree
    		throw new CompanyHierarchyException("Id already used!");
    	}
    	// check if new employee is already in tree
    	if (getEmployee(root, newEmployee.getId(), newEmployee.getName()) != null) {
    		throw new CompanyHierarchyException("Replacing employee already exists on the Company Tree!");
    	}
    	TreeNode oldEmployee = getEmployee(root, id, name);
    	if (oldEmployee == null) { // check if old employee is in tree
    		return false;
    	}
    	// check if new employee's title matches old employee's title
    	if (!oldEmployee.getEmployee().getTitle().equals(newEmployee.getTitle())){
    		throw new CompanyHierarchyException("Replacement title does not match existing title!");
    	}
    	// create new node for the new employee
    	TreeNode newNode = new TreeNode(newEmployee, oldEmployee.getSupervisor());
    	// handles if old worker is a leaf in the tree
    	if (oldEmployee.getWorkers().isEmpty()) {
    		newNode.updateSupervisor(oldEmployee.getSupervisor());
    		List<TreeNode> list = oldEmployee.getSupervisor().getWorkers();
    		list.add(newNode);
    		for (int i = 0; i < list.size(); i++) {
    			if (list.get(i).equals(oldEmployee)) {
    				list.remove(i);
    			}
    		}
    	}
    	else if (oldEmployee.equals(root)) { // handles if old worker is the root
    		List<TreeNode> workers = oldEmployee.getWorkers();
    		/* set new employee's workers to be same as old employee's 
    		 * workers and updates the workers' supervisor */
    		for (int i = 0; i < workers.size(); i++) {
    			newNode.addWorker(workers.get(i));
    			workers.get(i).updateSupervisor(newNode);
    		}
    		root = newNode;
    	}
    	else { // handles if old worker is neither a leaf or root in the tree
    		List<TreeNode> workers = oldEmployee.getWorkers();
    		/* set new employee's workers to be same as old employee's 
    		 * workers and updates the workers' supervisor */
    		for (int i = 0; i < workers.size(); i++) {
    			newNode.addWorker(workers.get(i));
    			workers.get(i).updateSupervisor(newNode);
    		}
    		List<TreeNode> list = oldEmployee.getSupervisor().getWorkers();
    		/* remove old employee from their supervisor's
    		 * list of workers */
    		for (int i = 0; i < oldEmployee.getSupervisor().getWorkers().size(); i++) {
    			if (list.get(i).equals(oldEmployee)) {
    				list.remove(i);
    			}
    		}
    		list.add(newNode);
    	}
    	return true;
    }

	/** Search and return the list of employees with the provided title; if none 
        *  found return null */
	public List<Employee> getEmployeeWithTitle(String title) {
		nodeList = new ArrayList<Employee> ();
		return getEmployeeWithTitle(root, title); // call private helper method
	}
	/** Start searching from the root and return the list of employees with  
     	*  the provided title; if none found return null */
	private List<Employee> getEmployeeWithTitle(TreeNode n, String title) {
		if (n == null) { // null check
			return nodeList;
		}
		if (n.getEmployee().getTitle().equals(title)) {
			nodeList.add(n.getEmployee()); // add employee to list if it's title matches
		}
		Iterator<TreeNode> itr = n.getWorkers().iterator(); // get iterator for list of workers
		while (itr.hasNext()) {
			getEmployeeWithTitle(itr.next(), title);
		}
		return nodeList;
	}

	/** Search and return the list of employees with date of joining within the 
        *  provided range; if none found return null 
	 * @throws ParseException */
	public List<Employee> getEmployeeInJoiningDateRange(String startDate, 
                                                String endDate) throws ParseException {
		nodeList = new ArrayList<Employee> ();
		return getEmployeeInJoiningDateRange(root, startDate, endDate); // calls private helper method
	}
	/** Start searching from the root and return the list of employees with date of joining 
     	*  within the  provided range; if none found return null */	
	private List<Employee> getEmployeeInJoiningDateRange(TreeNode n, String startDate, String endDate) {
		if (n == null) { // null check
			return nodeList;
		}
		try { // handles date formating
			SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy");
			Date start = parser.parse(startDate);
			Date end = parser.parse(endDate);
			Date join = parser.parse(n.getEmployee().getDateOfJoining());
			// check if joining date is between start and end date
			if (join.after(start) && join.before(end)) { 
				nodeList.add(n.getEmployee()); 
			}
			// check if joining date is equal to either date
			else if (join.equals(start) || join.equals(end)) {
				nodeList.add(n.getEmployee());
			}
			Iterator<TreeNode> itr = n.getWorkers().iterator(); // get iterator for list of workers
			while (itr.hasNext()) {
				getEmployeeInJoiningDateRange(itr.next(), startDate, endDate);
			}
		} catch (ParseException e) {
			throw new CompanyHierarchyException("Date parsing failed!");
		}	
		return nodeList;
	}

	/** Return the list of employees who are in the same level as the given 
        *  employee sharing the same supervisor */
	public List<Employee> getCoWorkers(int id, String name) {
		nodeList = new ArrayList<Employee> ();
		TreeNode employee = getEmployee(root, id, name);
		if (employee == null) { // check if employee is in tree
			return null;
		}
		if (employee.equals(root)) { // check if employee is root
			return nodeList;
		}
		List<TreeNode> workerList = employee.getSupervisor().getWorkers();
		for (int i = 0; i < workerList.size(); i++) {
			// add all of employee's supervisor's workers except the employee to the list
			if (!workerList.get(i).getEmployee().equals(employee.getEmployee())) {
				nodeList.add(workerList.get(i).getEmployee());
			}
		}
		return nodeList;
	}


	/** Returns the supervisor list(till CEO) for a given employee */
	public List<Employee> getSupervisorChain(int id, String name) {
		nodeList = new ArrayList<Employee> ();
		TreeNode employee = getEmployee(root, id, name);
		if (employee == null) { // check if employee is in the tree
			throw new CompanyHierarchyException("Employee not found!");
		}
		if (employee.equals(root)) { // check if employee is the root
			throw new CompanyHierarchyException("No Supervisor Chain found for that employee!");
		}
		return getSupervisorChain(employee.getSupervisor()); // calls private helper method
	}
	/** Returns the supervisor list(till CEO) starting at the employee node 
	 	* passed in for a given employee from the public method; returns null
	 	* if no such employee node exists */
	private List<Employee> getSupervisorChain(TreeNode n) {
		if (n == null) { // null check
			return nodeList;
		}
		nodeList.add(n.getEmployee()); // add employee to list
		getSupervisorChain(n.getSupervisor());
		return nodeList;
	}
}