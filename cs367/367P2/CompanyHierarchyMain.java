// Title:            Program 2
// Files:            CompanyHierarchyMain.java, CompanyHierarchy.java, 
//					 CompanyHierarchyException.java, Employee.java, TreeNode.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

/** The CompanyHierarchyMain class constructs the initial tree based on the 
 * input text file and allows the user to update the tree.
 * 
 *  @author Camron Johnson
 */

import java.util.*;
import java.io.*;
import java.text.ParseException;

public class CompanyHierarchyMain {
	
	// The first index of name
	private static final int NAME_INDEX1 = 0;
	
	// The first index of name
	private static final int NAME_INDEX2 = 1;

	// The first index of id
	private static final int ID_INDEX1 = 1;
	
	// The second index of id
	private static final int ID_INDEX2 = 0;
	
	// The first index of new id
	private static final int NEW_ID_INDEX = 0;
	
	// The second index of new id
	private static final int NEW_ID_INDEX2 = 2;

	// The first index of date
	private static final int DATE_INDEX = 2;
	
	// The second index of date
	private static final int DATE_INDEX2 = 4;

	// The first index of title
	private static final int TITLE_INDEX = 3;
	
	// The second index of title
	private static final int TITLE_INDEX2 = 5;

	// The first index of supervisor name
	private static final int SUPERVISOR_NAME_INDEX1 = 4;
	
	// The second index of supervisor name
	private static final int SUPERVISOR_NAME_INDEX2 = 5;

	// The first index of supervisor id
	private static final int SUPERVISOR_ID_INDEX1 = 5;

	// The second index of supervisor id
	private static final int SUPERVISOR_ID_INDEX2 = 4;
	
	// The first index of new name
	private static final int NEW_NAME_INDEX = 1;
	
	// The second index of new name
	private static final int NEW_NAME_INDEX2 = 3;

	// The index of start date
	private static final int START_DATE_INDEX = 0;

	// The index of end date
	private static final int END_DATE_INDEX = 1;

	private static CompanyHierarchy checkInputAndReturnTree (String [] args) {
		// *** Step 1: Check whether exactly one command-line argument is given ***
		if (args.length != 1) {
			System.out.println("Usage: java -cp . CompanyHierarchyMain FileName");
			System.exit(0);
		}
		// *** Step 2: Check whether the input file exists and is readable ***
		File inFile = new File(args[0]);
		if (!inFile.exists() || !inFile.canRead()) { // checks if file exists and is readable
			System.out.println("Error: Cannot access input file");
			System.exit(0);
		}

		/* Step 3: Load the data from the input file and use it to 
		 *  construct a company tree. Note: people are to be added to the 
		 *  company tree in the order in which they appear in the text file. 
		 */
		Scanner input = null;
		CompanyHierarchy companyTree = new CompanyHierarchy(); // creates company tree
		try {
			// use Scanner to read from the file
			input = new Scanner( inFile);
			// if there is a line to read, process it
			if (input.hasNextLine()) {
				String line = input.nextLine();
				String [] parts = line.split(",");
				String name = parts[NAME_INDEX1].trim();
				int id = Integer.parseInt(parts[ID_INDEX1].trim());
				String date = parts[DATE_INDEX].trim();
				String title = parts[TITLE_INDEX].trim();
				Employee ceo = new Employee(name, id, date, title);
				companyTree.addEmployee(ceo, 0, null);
			}
			// process all lines after the first line a little differently
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String [] parts = line.split(",");
				String name = parts[NAME_INDEX1].trim();
				int id = Integer.parseInt(parts[ID_INDEX1].trim());
				String date = parts[DATE_INDEX].trim();
				String title = parts[TITLE_INDEX].trim();
				String supName = parts[SUPERVISOR_NAME_INDEX1].trim();
				int supId = Integer.parseInt(parts[SUPERVISOR_ID_INDEX1].trim());
				Employee employee = new Employee(name, id, date, title);
				companyTree.addEmployee(employee, supId, supName);
			}
		}
		catch (FileNotFoundException e) { // print error if file not found
			System.out.println("Error: Cannot access file");
		}	
		return companyTree;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CompanyHierarchy tree = checkInputAndReturnTree(args);

		/* Step 4: Prompt the user to enter command options and 
		 *  process them until the user types x for exit. 
		 */
		boolean stop = false;

		Scanner stdin = new Scanner(System.in);
		while (!stop) {

			System.out.println("\nEnter Command: ");
			String input = stdin.nextLine();
			String remainder = null;
			//if the user enters more than one letter, the extra command is set as remainder
			if (input.length() > 0) {
				char option = input.charAt(0);
				if (input.length() > 1) {
					remainder = input.substring(1).trim();
				}

				//switch statement for the letter/command entered from the user
				switch (option) {

				/** a newid,newname,DOJ,title,supervisorId,supervisorName		
				 * Add a new employee with given details to the company tree. 
				 * Display "Employee added" if the addition was successful. 
				 * If there is no such supervisor in the company tree, 
				 * display "Cannot add employee as supervisor was not found!"
				 */
				case 'a': {
					// process command
					String [] info = remainder.split(",");
					int newId = Integer.parseInt(info[NEW_ID_INDEX]);
					String newName = info[NEW_NAME_INDEX];
					String date = info[DATE_INDEX];
					String title = info[TITLE_INDEX];
					int supId = Integer.parseInt(info[SUPERVISOR_ID_INDEX2]);
					String supName = info[SUPERVISOR_NAME_INDEX2];
					
					// create new employee
					Employee newEmployee = new Employee(newName, newId, date, title);
					
					try {
						// checks to see if employee was successfully added
						boolean added = tree.addEmployee(newEmployee, supId, supName);
						if (added) {
							System.out.println("Employee added");
						}
						/* if it wasn't added and the supervisor exists in the tree, 
						 * that means the employee already exist */
						else if (!added && tree.contains(supId, supName, "Incorrect supervisor name for id!")) {
							System.out.println("Employee already exists!");
						}
						else { // otherwise, supervisor was not found
							System.out.println("Cannot add employee as supervisor was not found!");
						}
					}
					catch (CompanyHierarchyException e) {
						System.out.println(e.getMessage());
					}
					break;
				}

				/** s id name		Print the name(s) of all the 
				 * supervisors in the supervisor chain of the given 
				 * employee. Print the names on separate lines. 
				 * If no such employee is found, display 
				 * "Employee not found!"*/	
				case 's':{
					// process command
					String [] info = remainder.split(",");
					int id = Integer.parseInt(info[ID_INDEX2]);
					String name = info[NAME_INDEX2];
					try {
						// create list of supervisors
						List<Employee> supervisors = tree.getSupervisorChain(id, name);

						if (supervisors == null) { // null check 
							System.out.println("Employee not found!");
						}
						for (int i = 0; i < supervisors.size(); i++) {
							System.out.println(supervisors.get(i).getName()); // print supervisors
						}
					}
					catch (CompanyHierarchyException e) {
						System.out.println(e.getMessage());
					}
					break;
				}

				/** d		Display information about the company tree 
				 * by doing the following:
				 * Display on a line: "# of employees in company tree: integer"
				 * This is the number of employees in this company tree.
				 *
				 * Display on a line: "max levels in company tree: integer"
				 * This is the maximum number of levels in the company tree.
				 *
				 * Display on a line: "CEO: name"
				 * This is the CEO in the company tree*/
				case 'd': {
					System.out.println("# of employees in company hierarchy tree: " 
							+ tree.getNumEmployees());
					System.out.println("max levels in company hierarchy tree: " 
							+ tree.getMaxLevels());
					System.out.println("CEO: " + tree.getCEO()); 

					break;
				}

				/** e title		Print the name(s) of the employee(s) 
				 *  that has the given title. Print the names on 
				 *  separate lines. If no such employee is found, 
				 *  display "Employee not found!" */
				case 'e': {
					// create list of employees
					List<Employee> employees = tree.getEmployeeWithTitle(remainder);
					if (employees.size() == 0) {
						System.out.println("Employee not found!");
					}
					for (int i = 0; i < employees.size(); i++) {
						System.out.println(employees.get(i).getName()); // print name of employees
					}
					break;
				}

				/** r id name		Remove the employee with given id 
				 * and name from the company tree and re-assign the 
				 * worker's to the removed employee's supervisor. 
				 * Display "Employee removed" after the removal. 
				 * If there is no such employee in the company tree, 
				 * display "Employee not found!" */
				case 'r': {
					// process command
					String [] info = remainder.split(",");
					int id = Integer.parseInt(info[ID_INDEX2]);
					String name = info[NAME_INDEX2];
					
					try {
						boolean removed = tree.removeEmployee(id, name);
						if (!removed) {
							System.out.println("Employee not found!");
						}
						else {
							System.out.println("Employee removed");
						}
					}
					catch (CompanyHierarchyException e) {
						System.out.println(e.getMessage());
					}
					break;
				}

				/** c id name		Print the name(s) of the 
				 * co-employees(sharing the same supervisor) of the 
				 * employee with given id and name. Print the names on 
				 * separate lines. If no such employee is found, 
				 * display "Employee not found!". If the employee has 
				 * no co-employee under the same supervisor, display 
				 * "The employee has no co-workers." */
				case 'c': {
					// process command
					String [] info = remainder.split(",");
					int id = Integer.parseInt(info[ID_INDEX2]);
					String name = info[NAME_INDEX2];
					
					try {
						// create list of coworkers
						List<Employee> coworkers = tree.getCoWorkers(id, name);
						if (coworkers == null) { // check if employee found
							System.out.println("Employee not found!");
							break;
						}
						if (coworkers.size() == 0) {
							System.out.println("The employee has no co-workers.");
							break;
						}
						for (int i = 0; i < coworkers.size(); i++) {
							System.out.println(coworkers.get(i).getName()); // print names of coworkers
						}
					}
					catch (CompanyHierarchyException e) {
						System.out.println(e.getMessage());
					}
					break;
				}

				/** u id name newid newname DOJ title		Replace the 
				 * employee with give id and name from the company tree 
				 * with the provided employee details. 
				 * Display "Employee replaced" after the removal. If 
				 * there is no such employee in the company tree, 
				 * display "Employee not found!" */
				case 'u': {
					// process command
					String [] info = remainder.split(",");
					int id = Integer.parseInt(info[ID_INDEX2]);
					String name = info[NAME_INDEX2];
					int newId = Integer.parseInt(info[NEW_ID_INDEX2]);
					String newName = info[NEW_NAME_INDEX2];
					String date = info[DATE_INDEX2];
					String title = info[TITLE_INDEX2];
					
					// create new employee
					Employee newEmployee = new Employee(newName, newId, date, title);
					
					try {
						boolean replaced = tree.replaceEmployee(id, name, newEmployee);
						if (!replaced) {
							System.out.println("Employee not found!");
						}
						else {
							System.out.println("Employee replaced");
						}
					}
					catch (CompanyHierarchyException e) {
						System.out.println(e.getMessage());
					}
					break;
				}

				/** j startDate endDate		Print the name(s) of the 
				 * employee(s) whose date of joining are between 
				 * startDate and endDate(you may assume that startDate 
				 * is equal to or before end date). Print the names on 
				 * separate lines. If no such employee is found, 
				 * display "Employee not found!" */
				case 'j': {
					// process command
					String [] info = remainder.split(",");
					String startDate = info[START_DATE_INDEX];
					String endDate = info[END_DATE_INDEX];
					List<Employee> employees = null;
					
					try {
						// create list of employees in joining date range
						employees = tree.getEmployeeInJoiningDateRange(startDate, endDate);
						if (employees.size() == 0) {
							System.out.println("Employee not found!");
						}
						for (int i = 0; i < employees.size(); i++) {
							System.out.println(employees.get(i).getName()); // print name of employees in that range
						}
					} catch (CompanyHierarchyException e) {
						System.out.println(e.getMessage());
					} catch (ParseException e) {
						e.printStackTrace();
					} 
					break;
				}

				//***exits program***
				case 'x':{
					stop = true;
					System.out.println("exit");
					break;
				}
				default:
					break;
				}
			}
		}
	}
}
