// Main Class: 		 CompanyHierarchyMain.java
// File:		     CompanyHierarchyException.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

/**
 * Stores the message of the exception
 *
 * @author Camron Johnson
 */

public class CompanyHierarchyException extends RuntimeException {

	private String msg;
	
	/**
	 * Constructs an exception with given message
	 *
	 * @param msg is the message of the exception
	 */
	public CompanyHierarchyException(String msg) {
		this.msg = msg;
	}
	
	/**
	 * Get's the exception's message
	 * 
	 * @return the exception's method
	 */
	public String getMessage() {
		return msg;
	}
}
