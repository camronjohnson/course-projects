// Main Class: 		 Game.java
// File:		     SpyGraph.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

import java.util.*;
/**
 * Stores all vertexes as a list of GraphNodes.  Provides necessary graph operations as
 * need by the SpyGame class.
 */
public class SpyGraph implements Iterable<GraphNode> {

	// list of all nodes in the graph
    private List<GraphNode> vlist;

    /**
     * Initializes an empty list of GraphNode objects
     */
    public SpyGraph(){
        vlist = new ArrayList<GraphNode>();
    }

    /**
     * Add a vertex with this label to the list of vertexes.
     * No duplicate vertex names are allowed.
     * @param name The name of the new GraphNode to create and add to the list.
     */
    public void addGraphNode(String name){
    	
    	boolean duplicate = false;
    	
    	// check to see if node with given name is in list of nodes
    	for (int i = 0; i < vlist.size(); i++) {
    		if (vlist.get(i).getNodeName().equals(name)) {
    			duplicate = true;
    		}
    	}
    	
    	// if node with given name is not in the list, add it
    	if (!duplicate) {
    		GraphNode newVertex = new GraphNode(name);
    		vlist.add(newVertex);
    	}
    }

    /**
     * Adds v2 as a neighbor of v1 and adds v1 as a neighbor of v2.
     * Also sets the cost for each neighbor pair.
     *   
     * @param v1name The name of the first vertex of this edge
     * @param v2name The name of second vertex of this edge
     * @param cost The cost of traveling to this edge
     * @throws IllegalArgumentException if the names are the same
     */
    public void addEdge(String v1name, String v2name, int cost) 
                throws IllegalArgumentException {
    	
    	// check to make sure nodes have different names
    	if (v1name.equals(v2name)) {
    		throw new IllegalArgumentException();
    	}
    	
    	// get nodes with give names
    	GraphNode v1 = getNodeFromName(v1name);
    	GraphNode v2 = getNodeFromName(v2name);
    	
    	// add nodes as eachother's neighbor 
    	v1.addNeighbor(v2, cost);
    	v2.addNeighbor(v1, cost);
    }

    /**
     * Return an iterator through all nodes in the SpyGraph
     * @return iterator through all nodes in alphabetical order.
     */
    public Iterator<GraphNode> iterator() {
        return vlist.iterator();
    }

    /**
     * Return Breadth First Search list of nodes on path 
     * from one Node to another.
     * @param start First node in BFS traversal
     * @param end Last node (match node) in BFS traversal
     * @return The BFS traversal from start to end node.
     */
    public List<Neighbor> BFS( String start, String end ) {
    	
    	// gets nodes
    	GraphNode startNode = getNodeFromName(start);
    	GraphNode endNode = getNodeFromName(end);
    	
    	// list to hold reverse path
    	List<GraphNode> list1 = new ArrayList<GraphNode>();
    	
    	// list to hold path to be returned
    	List<Neighbor> list2 = new ArrayList<Neighbor>();
    	
    	// queue to maintain while doing BFS
    	Queue<GraphNode> queue = new LinkedList<GraphNode>();
    	
    	// map to store nodes' predecessors
    	Map<GraphNode, GraphNode> map = new HashMap<GraphNode, GraphNode>();
    	   	
    	// mark node as visited and add it to the queue
    	startNode.setVisited(true);
    	queue.add(startNode);
    	
    	while (!queue.isEmpty()) {
    		
    		// remove current node from queue
    		GraphNode curr = queue.poll();
    		
    		// end loop if end node is found
    		if (curr.equals(endNode)) {
    			break;
    		}
    		
    		/* if node has not been visited, mark it as visited, 
    		 * add it to the queue, and add it to the map */
    		for (int i = 0; i < curr.getNeighbors().size(); i++) {
    			if (!curr.getNeighbors().get(i).getNeighborNode().getVisited()) {
	    			curr.getNeighbors().get(i).getNeighborNode().setVisited(true);
	    			queue.add(curr.getNeighbors().get(i).getNeighborNode());
	    			map.put(curr.getNeighbors().get(i).getNeighborNode(), curr);
    			}
    		}
    	}
    	
    	// create backwards list of path
    	GraphNode parent;
    	list1.add(endNode);
    	do {
    		parent = map.get(endNode);
    		list1.add(parent);
    		endNode = parent;
    	} while (!parent.equals(startNode));
    	
    	// reverse the order of the backwards path to get correct path
    	for (int i = list1.size() - 1; i > 0; i--) {
    		List<Neighbor> neighbors = list1.get(i).getNeighbors();
    		for (int j = 0; j < neighbors.size(); j++) {
    			if (neighbors.get(j).getNeighborNode().equals(list1.get(i-1))) {
    				list2.add(neighbors.get(j));
    			}
    		}
    	}
    	
    	setFalse(startNode);
    	
        return list2;
    }

    
    /**
     * This method sets all nodes' visited fields to false
     *
     * @param node current GraphNode
     */
    private void setFalse(GraphNode node) {
    	if (!node.getVisited()) {
    		return;
    	}
    	node.setVisited(false);
    	List<Neighbor> neighbors = node.getNeighbors();
    	for (int i = 0; i < neighbors.size(); i++) {
    		setFalse(neighbors.get(i).getNeighborNode());
    	}
    }

    /**
     * @param name Name corresponding to node to be returned
     * @return GraphNode associated with name, null if no such node exists
     */
    public GraphNode getNodeFromName(String name){
        for ( GraphNode n : vlist ) {
            if (n.getNodeName().equalsIgnoreCase(name))
                return n;
        }
        return null;
    }

    /**
     * Return Depth First Search list of nodes on path 
     * from one Node to another.
     * @param start First node in DFS traversal
     * @param end Last node (match node) in DFS traversal
     * @return The DFS traversal from start to end node.
     */
    public List<Neighbor> DFS(String start, String end) {

    	GraphNode startNode = getNodeFromName(start);
    	GraphNode endNode = getNodeFromName(end);
    	
    	List<Neighbor> list = new ArrayList<Neighbor>();
    	Stack<GraphNode> stack = new Stack<GraphNode>();
    	
    	DFS(startNode, endNode, list, stack);
    	
    	setFalse(startNode);
    	
    	return list;
    }
    
    
    /**
     * Determines path between nodes using depth-first-search
     *
     * @param curr current node
     * @param endNode the node we are looking for
     * @param list holds list of neighbors
     * @param stack keeps track of nodes
     * @return returns true or false based on whether or not the endNode was found
     */
    private boolean DFS(GraphNode curr, GraphNode endNode, 
    		List<Neighbor> list, Stack<GraphNode> stack) {
    	
    	// set node as visited and add it to the stack
    	curr.setVisited(true);
    	stack.push(curr);
    	
    	// if endNode is found, return true
    	if (curr.equals(endNode)) {
    		return true;
    	}
    	
    	Iterator<Neighbor> itr = curr.getNeighbors().iterator();
    	boolean found = false;
    	
    	while(itr.hasNext()) {
    		
    		// end loop if endNode is found
    		if (found) {
    			break;
    		}
			Neighbor neighbor = itr.next();
			if(!neighbor.getNeighborNode().getVisited()) {
				// if neighbor has not been visited, add it to the list
		    	list.add(neighbor);
				found = DFS(neighbor.getNeighborNode(), endNode, list, stack);
				// if endNode was not found, remove it from the list and pop it from the stack
				if(!found) {
					list.remove(list.size() - 1);
					stack.pop();
				}
			}
		}
    	return found;
    }

    /**
     * OPTIONAL: Students are not required to implement Dijkstra's ALGORITHM
     *
     * Return Dijkstra's shortest path list of nodes on path 
     * from one Node to another.
     * @param start First node in path
     * @param end Last node (match node) in path
     * @return The shortest cost path from start to end node.
     */
    public List<Neighbor> Dijkstra(String start, String end){

        // TODO: implement Dijkstra's shortest path algorithm
        // may need and create a companion method
        
    	List<Neighbor> list = new ArrayList<Neighbor>();
    	
        return list;
    }

    /**
     * DO NOT EDIT THIS METHOD 
     * @return a random node from this graph
     */
    public GraphNode getRandomNode() {
        if (vlist.size() <= 0 ) {
            System.out.println("Must have nodes in the graph before randomly choosing one.");
            return null;
        }
        int randomNodeIndex = Game.RNG.nextInt(vlist.size());
        return vlist.get(randomNodeIndex);
    }
}