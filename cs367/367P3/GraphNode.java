// Main Class: 		 Game.java
// File:		     GraphNode.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

import java.util.*;

/**
 * GraphNode class maintains a vertex name and a list of adjacent vertices which
 * stores the neighbors in a sorted list.
 */

public class GraphNode implements Comparable<GraphNode> {
   
	// name of node
	private String nodeName;
	
	// keeps track of whether or not node has a spycam
    private boolean spycam;
    
    // list of neighbors to the node
    private List<Neighbor> neighbors;
    
    // keeps track of whether or not node has been visited
    private boolean visited;
    
    /** 
     * Constructs a GraphNode with the vertex name and empty neighbors list.
     * 
     * @param name of the vertex, which needs to be unique
     */
    public GraphNode(String name) {
    	nodeName = name;
    	neighbors = new ArrayList<Neighbor>();
    }

    /** 
     * Returns the name of the vertex. 
     *
     * @return the name of this GraphNode
     */
    public String getNodeName() {
    	return nodeName;
    }

    /** 
     * Returns the neigbors of the vertex. 
     *
     * @return the neighbors of this GraphNode
     */
    public List<Neighbor> getNeighbors() {
    	return neighbors;
    }

    /**
     * Sets the visited flag of this vertex.
     *
     * @param flagVal boolean value used to set the flag
     */
    public void setVisited(boolean flagVal) {
    	visited = flagVal;
    }
    
    /**
     * Gets the visited flag of this vertex.
     *
     * @return visited boolean value 
     */
    public boolean getVisited() {
    	return visited;
    }

    /** 
     * Return the results of comparing this node's name to the other node's 
     * name. 
     * 
     * @param otherNode GraphNode instance whose vertex name is required for 
     * comparison
     * @return negative value or 0 or positive value
     */
    public int compareTo(GraphNode otherNode) {
    	int result = nodeName.compareTo(otherNode.getNodeName());
    	return result;
    }


    /** 
     * Adds a new neighbor and maintains sorted order of neighbors by neighbor 
     * name.
     *
     * @param neighbor an adjacent node 
     * @param cost to move to that node (from this node)
     */
    public void addNeighbor(GraphNode neighbor, int cost) {
    	neighbors.add(new Neighbor(cost, neighbor));
    	neighbors.sort(null);
    }

    /** 
     * Prints a list of neighbors of this GraphNode and the cost of the edge to 
     * them. 
     * Example:
     * "1 b"
     * "4 c"
     * Note: Quotes are given here for clarification, do not print the quotes.
     */
    public void displayCostToEachNeighbor() {
    	for(int i = 0; i < neighbors.size(); i++) {
    		System.out.println("" + neighbors.get(i).getCost() + " " 
    				+ neighbors.get(i).getNeighborNode().getNodeName()); 	
    	}
    }

    /** 
     * Returns cost to reach the neighbor.
     *
     * @param neighborName name of neighbor
     * @return cost to neighborName
     * @throws NotNeighborException if neighborName is not a neighbor
     */
    public int getCostTo(String neighborName) throws NotNeighborException {
    	Iterator<String> itr = getNeighborNames();
    	int index = 0;
    	
    	/* looks for name of neighbor in list of neighbors and if found, 
    	 * return the cost to travel to that neighbor */    	
    	while (itr.hasNext()) {
    		if (itr.next().equals(neighborName)) {
    			return neighbors.get(index).getCost();
    		}
    		index++;
    	}
    	
    	// if neighbor is not found, thrown exception
    	throw new NotNeighborException();
    }

    /** 
     * Returns the GraphNode associated with name that is a neighbor of the 
     * current node.
     *
     * @param neighborName name of neighbor
     * @return the GraphNode associated with name that is neighbor of this node
     * @throws NotNeighborException if neighborName is not a neighbor
     */
    public GraphNode getNeighbor(String neighborName) throws NotNeighborException {
    	Iterator<String> itr = getNeighborNames();
    	int index = 0;
    	
    	// looks for neighbor node with given name
    	while (itr.hasNext()) {
    		if (itr.next().equals(neighborName)) {
    			return neighbors.get(index).getNeighborNode();
    		}
    		index++;
    	}
    	throw new NotNeighborException();
    }

    /** 
     * Returns an iterator that can be used to find neighbor names
     * of this GraphNode.
     *
     * @return iterator of String node labels
     */
    public Iterator<String> getNeighborNames() {
    	List<String> neighborNames = new ArrayList<String>();
    	
    	// creates a list of neighbors' names
    	for (int i = 0; i < neighbors.size(); i++) {
    		neighborNames.add(neighbors.get(i).getNeighborNode().getNodeName());
    	}
    	return neighborNames.iterator();
    }

    /** 
     * Sets/unsets spycam at this node.
     *
     * @param cam indicates whether the node now has a spycam
     */
    public void setSpycam(boolean cam) {
    	spycam = cam;
    }

    /** 
     * Returns information about spycam presense in this node.
     *
     * @return true if the GraphNode has a spycam
     */
    public boolean getSpycam() {
    	return spycam;
    }

    /** 
     * Returns true if this node name is a neighbor of current node.
     *
     * @param neighborName name of neighbor
     * @return true if the node is an adjacent neighbor
     */
    public boolean isNeighbor(String neighborName) {
    	Iterator<String> itr = getNeighborNames();
    	
    	// looks for neighbor with given name
    	while (itr.hasNext()) {
    		if (itr.next().equals(neighborName)) {
    			return true;
    		}
    	}
    	
    	// return false if neighbor not found
    	return false;
    }

    /** 
     * Returns the name of this node.
     *
     * @return name of node
     */
    public String toString() {
    	return nodeName;
    }

}
