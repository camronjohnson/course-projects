// Main Class: 		 Game.java
// File:		     Neighbor.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

/**
 * Neighbor class represents an edge between two nodes and the associated cost.
 */

public class Neighbor implements Comparable<Neighbor> {
   
	// cost to travel between nodes
	private int cost;
    
	// node that is a neighbor
	private GraphNode neighbor;

    /** 
     * A neighbor is added to an existing GraphNode by creating an instance 
     * of Neighbor that stores the neighbor and the cost to reach that 
     * neighbor.
     * 
     * @param cost of the edge
     * @param neighbor GraphNode
     */
    public Neighbor(int cost, GraphNode neighbor) {
    	this.cost = cost;
    	this.neighbor = neighbor;
    }

    /** 
     * Returns the cost of the edge. 
     *
     * @return the cost of this edge.
     */
    public int getCost() {
    	return cost;
    }

    /** 
     * Returns the neigbor on the other end of the edge. 
     *
     * @return the neighbor
     */
    public GraphNode getNeighborNode() {
    	return neighbor;
    }

    /** 
     * Return the results of comparing this node's neighbor name to the other 
     * node's neighbor name. Allows List of Neighbors to be sorted. 
     * Hint: Read the java docs for String class carefully 
     *
     * @param otherNode Neighbor instance whose Graphnode needs to be compared.
     * @return negative value or 0 or positive value
     */
    public int compareTo(Neighbor otherNode) {
    	int result = neighbor.compareTo(otherNode.getNeighborNode());
    	return result;
    }

    /**
     * Returns a String representation of this Neighbor.
     * The String that is returned shows an arrow (with the cost in the middle) 
     * and then the Neighbor node's name. 
     *
     * Example:  
     * " --1--> b"
     * indicates a cost of 1 to get to node b
     * Note: Quotes are given here for clarification, do not print the quotes.
     *
     * @return String representation
     */
    public String toString() {
    	String stringRep = "--" + cost + "--> " + neighbor;
    	return stringRep;
    }
}
