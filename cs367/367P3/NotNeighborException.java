// Main Class: 		 Game.java
// File:		     NotNeighborException.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

public class NotNeighborException extends Exception {
}
