// Main Class: 		 Game.java
// File:		     Player.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

import java.util.*;

/**
 * Represents a player in the Game.
 */
public class Player {
	
	// name of player
    String playerName;
    
    // player's budget
    int budget;
    
    // number of spycams remaining
    int numSpycams;
    
    // keeps track of current node
    GraphNode currNode;
    
    // list of nodes that currently have a spycam
    List<GraphNode> spycams;

    /*
     * Constructs an instance of Player to track location and other information
     * for the player.
     */
    public Player(String name, int budget, int spycams, GraphNode startnode) {
    	playerName = name;
    	this.budget = budget;
    	numSpycams = spycams;
    	currNode = startnode;
    	this.spycams = new ArrayList<GraphNode>();
    }

    /*
     * Subtract the decrease amount from the budget.
     * 
     * @param dec amount to decrease the budget by
     */
    public void decreaseBudget(int dec) {
    	budget = budget - dec;
    }

    /*
     * If there are no remaining spy cams to drop, display "Not enough spycams" 
     * and return false. Otherwise:
     *
     * If there is not a spy cam at the player's current location:
     *  display "Dropped a Spy cam at D" where D is the node name
     *  drop a spy cam (here) D
     *  decrement the remaining spy cam count if there was not already a spycam
     *
     * Note: Make sure to set the spycam on the GraphNode as well.
     */
    public boolean dropSpycam() {
    	
    	// makes sure player has money
    	if (numSpycams == 0) {
    		System.out.println("Not enough spycams");
    		return false;
    	}
    	
    	// drops spycam if there is not one already at the node
    	if (!currNode.getSpycam()) {
    		System.out.println("Dropped a Spy cam at " + currNode.getNodeName());
    		currNode.setSpycam(true);
    		spycams.add(currNode);
    		numSpycams--;
    	}
    	
    	// does nothing if spycam is already at node
    	else {
    		System.out.println("Already a Spy cam there");
    	}
    	return true;
    }

    /*
     * Return the amount remaining in this player's budget.
     *
     * @return budget remaining
     */
    public int getBudget() {
    	return budget;
    }

    /*
     * Returns the node where the player is currently located.
     *
     * @return currNode player's current node 
     */
    public GraphNode getLocation() {
    	return currNode;
    }

    /*
     * Returns the name of the node where the player is currently located.
     *
     * @return node label for the current location of the player
     */
    public String getLocationName() {
    	return currNode.getNodeName();
    }

    /*
     * Return the name of the player.
     *
     * @return name of player
     */
    public String getName() {
    	return playerName;
    }

    /*
     * If pickupSpyCam is true, increment the number of spy cams remaining.
     * Note: Make sure to unset the spycam on the GraphNode as well.
     *
     * @param: pickupSpyCam true if a spy cam was picked up. 
     *         False means there was no spy cam
     */
    public void getSpycamBack(boolean pickupSpyCam) {
    	
    	// if input parameter is true, remove spycam from node
    	if (pickupSpyCam) {
    		numSpycams++;
    		currNode.setSpycam(false);
    	}	
    }

    /*
     * Returns the number of spy cams available to drop.
     *
     * @return number of spycams remaining
     */
    public int getSpycams() {
    	return numSpycams;
    }

    /*         
     * Change the location of the player to the specified node. Moves to nodes 
     * with a cost of one are not counted. (ie. it is "free" to "walk" to a 
     * node). 
     * If attempt to move to a node that is not a neighbor (nn) is made,
     * displays the message: "<nn> is not a neighbor of your current location"; 
     * and returns false.
     * If there is no sufficient budget, then display 
     * "Not enough money cost is <COST> budget is <BUDGET>" 
     * Note: Quotes are given here for clarification, do not print the quotes.
     * If the cost to neighbor is greater than 1, decrement budget by that 
     * amount.         
     *                 
     * Note: Write a try-catch block for NotNeighborException, but you should
     * do necessary checks such that this exception will never be thrown from
     * the GraphNode functions that you are invoking.
     * Hint: Carefully consider which function to call first to ensure that 
     * an exception will never be thrown.
     *                 
     * @param name of the neighboring to move to
     * @return true if the player successfully moves to this node
     */                

    public boolean move(String name) {
    	
    	boolean found = false;
    	GraphNode destination = null;
    	
    	// searches for node matching given name from the list of neighbors
    	for (int i = 0; i < currNode.getNeighbors().size(); i++) {
    		if (currNode.getNeighbors().get(i).getNeighborNode().getNodeName().equals(name)) {
    			found = true;
    			destination = currNode.getNeighbors().get(i).getNeighborNode();
    		}
    	}
    	
    	// if no neighbor node found, print that and return false
    	if (!found) {
    		System.out.println(name + " is not a neighbor of your current location");
    		return false;
    	}
    	
    	try {
    		/* if cost to neighbor is greater than one and player has sufficient 
    		 * budget, decrement cost from budget and move player */
	    	if (currNode.getCostTo(name) != 1) {
	    		if (currNode.getCostTo(name) > budget) {
	    			System.out.println("Not enough money cost is " + 
	    					currNode.getCostTo(name) + " budget is " + budget);
	    			return false;
	    		}
	    		budget = budget - currNode.getCostTo(name);
	    		currNode = destination;
	    		return true;
	    	}
	    	// if cost to neighbor is one, simply move player
	    	else {
	    		currNode = destination;
	    		return true;
	    	}
    	}
    	catch (NotNeighborException e) {
    		e.printStackTrace();
    	}
    	return false;
    }
    
    /*
     * Check the node to see if there is a spy cam. If there is a spy cam at 
     * that node, remove the spy cam from that node. Also, remove the spy cam 
     * name from the Player's list of spy cam names. Otherwise, return false.
     * Note: Make sure to unset the spycam on the GraphNode as well.
     *
     * @param node the player asked to remove a spy cam from.
     * @return true if a spycam is retrieved
     */
    public boolean pickupSpycam(GraphNode node) {
    	
    	/* if there's a spycam at the node, remove it 
    	 * and increment number of spycams remaining */
    	if(node.getSpycam()) {
    		node.setSpycam(false);
    		for (int i = 0; i < spycams.size(); i++) {
    			if (spycams.get(i).getNodeName().equals(node.getNodeName())) {
    				spycams.remove(i);
    				numSpycams++;
    				node.setSpycam(false);
    				return true;
    			}
    		}
    	}
    	return false;
    }

    /*
     * Display the names of the locations where Spy Cams were dropped (and are 
     * still there). If there are spy cams at nodes named a, f, and g, the 
     * display would show:
     * "Spy cam at a"
     * "Spy cam at f"
     * "Spy cam at g"
     * Note: Quotes are given here for clarification, do not print the quotes.
     */
    public void printSpyCamLocations() {
    	for (int i = 0; i < spycams.size(); i++) {
    		System.out.println("Spy cam at " + spycams.get(i).getNodeName());
    	}
    }
}
