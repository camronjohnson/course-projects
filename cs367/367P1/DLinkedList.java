// Main Class File:  Cloud.java
// File:             DLinkedList.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

/**
 * Implements the ListADT interface
 * 
 * @author Camron Johnson
 */
public class DLinkedList<E> implements ListADT<E>{
	
	// reference to first node in list
	private DblListnode<E> head;
	
	// numeric value to track number of nodes
	private int numItems;
	
	// reference  to last node in list
	private DblListnode<E> tail;
	
	/**
     * Initializes instance variables 
     */
	public DLinkedList() {
		head = null;
		numItems = 0;
		tail = null;
	}
	
	/**
     * Adds item to the end of the List.
     * 
     * @param item the item to add
     * @throws IllegalArgumentException if item is null 
     */
	public void add(E item) {
		// null check
		if (item == null) {
			throw new IllegalArgumentException();
		}
		DblListnode<E> newNode = new DblListnode<E>(item);
		// if list is empty, have head and tail reference point to new node
		if (head == null) {
			head = newNode;
			tail = newNode;
		}
		else {
			// if list is not empty, set current tail to point to new node
			tail.setNext(newNode);

			// set new node to point to current tail
			newNode.setPrev(tail);
			
			// set tail to point to new node
			tail = newNode;
		}
		numItems++;
	}

	 /**
     * Adds item at position pos in the List, moving the items originally in 
     * positions pos through size() - 1 one place to the right to make room.
     * 
     * @param pos the position at which to add the item
     * @param item the item to add
     * @throws IllegalArgumentException if item is null 
     * @throws IndexOutOfBoundsException if pos is less than 0 or greater 
     * than size()
     */
	public void add(int pos, E item) {
		// null check
		if (item == null) {
			throw new IllegalArgumentException();
		}
		
		// check for valid pos value
		if (pos < 0 || pos > size()) {
			throw new IndexOutOfBoundsException();
		}
		DblListnode<E> newNode = new DblListnode<E>(item);
		
		// reference to iterate through list
		DblListnode<E> curr = head;
		
		// if list is empty, have head and tail reference point to new node
		if (head == null) {
			head = newNode;
			tail = newNode;
			numItems++;
		}
		// if pos equals 0, add new node at front of list
		else if (pos == 0) {
			newNode.setNext(head);
			head.setPrev(newNode);
			head = newNode;
			numItems++;
		}
		
		// is pos equals size(), call add() method
		else if (pos == size()) {
			add(item);
		}
		else {
			// if pos > 0 and pos < size(), iterate to node before pos
			for (int i = 0; i < pos - 1; i++) {
				curr = curr.getNext();
			}
			
			// add new node to position pos
			newNode.setNext(curr.getNext());
			curr.setNext(newNode);
			newNode.setPrev(curr);
			newNode.getNext().setPrev(newNode);
			numItems++;
		}
	}

	/**
     * Returns true iff item is in the List (i.e., there is an item x in the 
     * List such that x.equals(item))
     * 
     * @param item the item to check
     * @return true if item is in the List, false otherwise
     * @throws IllegalArgumentException if item is null or list is null
     */
	public boolean contains(E item) {
		// null check
		if (head == null || item == null) {
			throw new IllegalArgumentException();
		}
		DblListnode<E> curr = head;
		// iterate through list looking for match
		while (curr != null) {
			if (item.equals(curr.getData())) {
				return true;
			}
			curr = curr.getNext();
		}
		return false;
	}

	/**
     * Returns the item at position pos in the List.
     * 
     * @param pos the position of the item to return
     * @return the item at position pos
     * @throws IllegalArgumentException if list is null
     * @throws IndexOutOfBoundsException if pos is less than 0 or greater than
     * or equal to size()
     */
	public E get(int pos) {
		// null check
		if (head == null) {
			throw new IllegalArgumentException();
		}
		// check for valid pos value
		if (pos < 0 || pos >= size()) {
			throw new IndexOutOfBoundsException();
		}
		DblListnode<E> curr = head;
		// iterate to node at position pos and return it
		for (int i = 0; i < pos; i++) {
			curr = curr.getNext();
		}
		return curr.getData();
	}

	/**
     * Returns true iff the List is empty.
     * 
     * @return true if the List is empty, false otherwise
     */
	public boolean isEmpty() {
		if (size() == 0) {
			return true;
		}
			return false;
	}

	/**
     * Removes and returns the item at position pos in the List, moving the 
     * items originally in positions pos+1 through size() - 1 one place to the 
     * left to fill in the gap.
     * 
     * @param pos the position at which to remove the item
     * @return the item at position pos
     * @throws IllegalArgumentException if list is null
     * @throws IndexOutOfBoundsException if pos is less than 0 or greater than
     * or equal to size()
     */
	public E remove(int pos) {
		// null check
		if (head == null) {
			throw new IllegalArgumentException();
		}
		// check for valid pos value
		if (pos < 0 || pos >= size()) {
			throw new IndexOutOfBoundsException();
		}
		DblListnode<E> curr = head;
		// variable to store removed node
		E removed = null;
		if (pos == 0) { // removes first node
			removed = head.getData();
			head = head.getNext();
			head.setPrev(null);
		}
		// removes node when pos equals size() - 1
		else if (pos == size() - 1) { 
			removed = tail.getData();
			tail.getPrev().setNext(null);
		}
		else {
			// removes node when pos is > 0 and pos < size() - 1
			for (int i = 0; i < pos - 1; i++) {
				curr = curr.getNext();
			}
			removed = curr.getNext().getData();
			curr.setNext(curr.getNext().getNext());
			curr.getNext().setPrev(curr);
		}
		numItems--;
		return removed;
	}

	/**
     * Returns the number of items in the List.
     * 
     * @return the number of items in the List
     */
	public int size() {
		return numItems;
	}	
}