// Title:            Program 1
// Files:            Cloud.java, DLinkedList.java, 
//					 InsufficientCreditException.java, Machine.java, User.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Main class which simulates the cloud environment.
 * 
 * @author Camron Johnson
 */
public class Cloud {

	// Store record of users and machines
	private static ListADT<Machine> machines = new DLinkedList<Machine>();
	private static ListADT<User> users = new DLinkedList<User>();
	private static User currentUser = null; // current user logged in

	// The index of the machine's name
	private static final int MACHINE_NAME_INDEX = 0;

	// The index of the number of CPUs
	private static final int NUMBER_OF_CPUS_INDEX = 1;

	// The index of memory size
	private static final int MEMORY_SIZE_INDEX = 2;

	// The index of disk size
	private static final int DISK_SIZE_INDEX = 3;

	// The index of the price
	private static final int PRICE_INDEX = 4;

	// The index of the user's name
	private static final int NAME_INDEX = 0;

	// The index of the user's password
	private static final int PASSWORD_INDEX = 1;

	// The index of the user's credit
	private static final int CREDIT_INDEX = 2;

	//scanner for console input
	public static final Scanner stdin = new Scanner(System.in);

	//main method
	public static void main(String args[]) {

		//Populate the two lists using the input files: Machines.txt User1.txt 
		//User2.txt ... UserN.txt
		if (args.length < 2) {
			System.out.println("Usage: java Cloud [MACHINE_FILE] [USER1_FILE] [USER2_FILE] ...");
			System.exit(0);
		}

		//load store machines
		loadMachines(args[0]);

		//load users one file at a time
		for(int i = 1; i< args.length; i++) {
			loadUser(args[i]);
		}

		//User Input for login
		boolean done = false;
		while (!done) 
		{
			System.out.print("Enter username : ");
			String username = stdin.nextLine();
			System.out.print("Enter password : ");
			String passwd = stdin.nextLine();

			if(login(username, passwd) != null)
			{
				//generate random items in stock based on this user's machine 
				//list
				ListADT<Machine> inStock = currentUser.generateMachineStock();
				//show user menu
				userMenu(inStock);
			}
			else
				System.out.println("Incorrect username or password");

			System.out.println("Enter 'exit' to exit program or anything else to go back to login");
			if(stdin.nextLine().equals("exit"))
				done = true;
		}

	}

	/**
	 * Tries to login for the given credentials. Updates the currentUser if 
	 * successful login
	 * 
	 * @param username name of user
	 * @param passwd password of user
	 * @returns the currentUser 
	 */
	public static User login(String username, String passwd) {  	
		for (int i = 0; i < users.size(); i++) {
			// if it is a valid login, update currentUser
			if (users.get(i).checkLogin(username, passwd)) {
				currentUser = users.get(i);
				return currentUser;
			}
		}   	
		return currentUser;
	}

	/**
	 * Reads the specified file to create and load machines into the store.
	 * Every line in the file has the format: <NAME>#<CATEGORY>#<PRICE>#<RATING>
	 * Create new machines based on the attributes specified in each line and 
	 * insert them into the machines list
	 * Order of machines list should be the same as the machines in the file
	 * For any problem in reading the file print: 'Error: Cannot access file'
	 * 
	 * @param fileName name of the file to read
	 */
	public static void loadMachines(String fileName) {
		// create new object to access information from fileName
		File inFile = new File( fileName);
		Scanner input = null;
		try {
			// use Scanner to read from the file
			input = new Scanner( inFile);
			// if there are lines to read, process them
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String [] parts = line.split("#");    	
				// split machine information into its corresponding parts
				int cpus = Integer.parseInt(parts[NUMBER_OF_CPUS_INDEX]);
				int memory = Integer.parseInt(parts[MEMORY_SIZE_INDEX]);
				int diskSize = Integer.parseInt(parts[DISK_SIZE_INDEX]);
				double price = Double.parseDouble(parts[PRICE_INDEX]);    			
				// add new machine with file specifications
				machines.add(new Machine(parts[MACHINE_NAME_INDEX], cpus, 
						memory, diskSize, price));
			}
		} catch (FileNotFoundException e) { // print error if file not found
			System.out.println("'Error: Cannot access file'");
		}
		finally {
			// close the Scanner when done
			if ( input != null) input.close();
		}
	}

	/**
	 * Reads the specified file to create and load a user into the store.
	 * The first line in the file has the format:<NAME>#<PASSWORD>#<CREDIT>
	 * Every other line after that is a name of a machine in the user's 
	 * machinelist, format:<NAME>
	 * For any problem in reading the file print: 'Error: Cannot access file'
	 * 
	 * @param fileName name of the file to read
	 */
	public static void loadUser(String fileName) {
		// create new object to access information from fileName
		File inFile = new File( fileName);
		Scanner input = null;
		try {
			// use Scanner to read from the file
			input = new Scanner( inFile);
			// if there is a line to read, process it
			if (input.hasNextLine()) {
				String line = input.nextLine();
				String [] parts = line.split("#"); 
				double credit = Double.parseDouble(parts[CREDIT_INDEX]);
				// add new user with file specifications
				users.add(new User(parts[NAME_INDEX], parts[PASSWORD_INDEX], 
						credit));
				login(parts[NAME_INDEX], parts[PASSWORD_INDEX]);
			}
			// if there are lines to read, process them
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String [] parts = line.split("#");  	
				// split machine information into its corresponding parts
				int cpus = Integer.parseInt(parts[NUMBER_OF_CPUS_INDEX]);
				int memory = Integer.parseInt(parts[MEMORY_SIZE_INDEX]);
				int diskSize = Integer.parseInt(parts[DISK_SIZE_INDEX]);
				double price = Double.parseDouble(parts[PRICE_INDEX]);
				// add new machine with file specifications
				currentUser.addToMachineList(new Machine(parts[MACHINE_NAME_INDEX], 
						cpus, memory, diskSize, price));
			}
		} catch (FileNotFoundException e) { // print error if file not found
			System.out.println("'Error: Cannot access file'");
		}
		finally {
			// close the Scanner when done
			if ( input != null) input.close();
		}
	}

	/** 
	 * Prints the entire machine inventory.
	 * The order of the machines should be the same as in input machines file.
	 * The output format should be the consolidated String format mentioned
	 * in Machine class.
	 */
	public static void printMachines() {
		for (int i = 0; i < machines.size(); i++) {
			System.out.println(machines.get(i).toString());
		}
	}

	/**
	 * Interacts with the user by processing commands
	 * 
	 * @param inStock list of machines that are in stock
	 */
	public static void userMenu(ListADT<Machine> inStock) {
		boolean done = false;
		while (!done) 
		{
			System.out.print("Enter option : ");
			String input = stdin.nextLine();

			//only do something if the user enters at least one character
			if (input.length() > 0) 
			{
				String[] commands = input.split(":");//split on colon, because 
				//names have spaces in them
				if(commands[0].length()>1)
				{
					System.out.println("Invalid Command");
					continue;
				}
				switch(commands[0].charAt(0)) {
				case 'v':
					if (commands[1].equals("all")) {
						printMachines(); // print machines in Cloud
					}
					else if (commands[1].equals("machinelist")) {
						// print user's machine list
						currentUser.printMachineList();
					}
					else if (commands[1].equals("instock")) {
						// print each machine that is in stock
						for (int i = 0; i < inStock.size(); i++) {
							System.out.println(inStock.get(i).toString());
						}
					}
					break;

				case 's':
					// make sure it's a string
					if (commands[1] instanceof String) { 
						for (int i = 0; i < machines.size(); i++) {
							/* Check if what the user entered is in any 
							 * of the names of the machines in the cloud. If it
							 * is, print that machine.
							 */
							if (machines.get(i).getName().
									contains(commands[1])) {
								System.out.println(machines.get(i).toString());
							}
						}
					}
					break;

				case 'a':
					// make sure it's a string
					if (commands[1] instanceof String) {
						// variable to track if match is found
						boolean match = false;
						for (int i = 0; i < machines.size(); i++) {
							// check if machine is in the Cloud 
							if (commands[1].equals(machines.get(i).
									getName())) {
								// if it is, add it to the user's machine list
								currentUser.addToMachineList(machines.get(i));
								match = true;
							}
						}
						if (!match) {
							System.out.println("Machine not found");
						}
					}
					break;

				case 'r':
					// make sure it's a string
					if (commands[1] instanceof String) {
						// variable to track if match is found
						boolean match = false;
						for (int i = 0; i < machines.size(); i++) {
							// check if machine is in the Cloud 
							if (commands[1].equals(machines.get(i).
									getName())) {
								// if it is, remove it from the user's machine list
								currentUser.removeFromMachineList(commands[1]);
								match = true;
							}
						}
						if (!match) {
							System.out.println("Machine not found");
						}
					}
					break;

				case 'b':
					for (int i = 0; i < inStock.size(); i++) {
						// variable to track if rent is successful
						boolean match = false;
						try {
							match = currentUser.rent(inStock.get(i).
									getName());
							// if rent unsuccessful, print message
							if (!match) {
								System.out.println("Machine not needed: " + 
							inStock.get(i).getName());
							}
						}
						// if insufficient credit, print message
						catch (InsufficientCreditException e) {
							System.out.println("For renting " + inStock.get(i).
									getName() + ": " + e.getMessage());
						}
					}
					break;

				case 'c':
					// reports credit
					System.out.println("$" + currentUser.getCredit());
					break;

				case 'l':
					// logs user out
					done = true;
					System.out.println("Logged Out");
					break;

				default:  //a command with no argument
					System.out.println("Invalid Command");
					break;
				}
			}
		}
	}
}