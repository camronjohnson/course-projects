// Main Class File:  Cloud.java
// File:             User.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

import java.util.Random;

/**
 * The User class uses DLinkedList to build a price ordered list called 
 * 'machineList' of machines.
 * Machines with higher price fields should come earlier in the list.
 * 
 * @author Camron Johnson
 */
public class User {
    // Random number generator, used for generateMachineStock. DO NOT CHANGE
    private static Random randGen = new Random(1234);
    
    // The username for the User
    private String username;
    
    // The password for the User
    private String passwd;
    
    // The credit for the User
    private double credit;
    
    // The User's machineList
    private ListADT<Machine> machineList;
    
    /**
     * Constructs a User instance with a name, password, credit and an empty 
     * machineList. 
     * 
     * @param username name of user
     * @param passwd password of user
     * @param credit amount of credit the user had in $ 
     */
    public User(String username, String passwd, double credit) {
    	this.username = username;
    	this.passwd = passwd;
    	this.credit = credit;
    	machineList = new DLinkedList<Machine>();
    }
    
    /**
     * Checks if login for this user is correct.
     *
     * @param username the name to check
     * @param passwd the password to check
     * @return true if credentials correct, false otherwise
     * @throws IllegalArgumentException if arguments are null 
     */
    public boolean checkLogin(String username, String passwd) {
    	// null check
    	if (username == null || passwd == null) {
    		throw new IllegalArgumentException();
    	}
    	// check for valid login by comparing username and password
    	if (username.equals(this.username) && passwd.equals(this.passwd)) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * Adds a machine to the user's machineList. 
     * Maintain the order of the machineList from highest priced to lowest 
     * priced machines.
     * @param machine the Machine to add
     * @throws IllegalArgumentException if arguments are null 
     */
    public void addToMachineList(Machine machine) {
    	// null check 
    	if (machine == null) {
    		throw new IllegalArgumentException();
    	}
		// if the user's machineList is empty, add the machine to the empty list
    	if (machineList.isEmpty()) {
    		machineList.add(machine);
    	}
    	else {
    		// flag to track if machine is most expensive machine
    		boolean mostExpensive = true; 
    		
    		/* numeric value to track the location of where the machine 
    		 * should be added 
    		 */
    		int location = 0;
    		for (int i = 0; i < machineList.size(); i++) {
    			// compare machine price to each machine in the list
    			if (machine.getPrice() <= machineList.get(i).getPrice()) {
    				mostExpensive = false;
    				location = i;
    			}
    		}
    		/* if machine is the most expensive, add it to the 
    		 * beginning of the list 
    		 */
    		if (mostExpensive) {
    			machineList.add(0, machine);
    		}
    		/* if not the most expensive, add to the right of last machine 
    		 * that is more expensive
    		 */    		
    		else {
    			machineList.add(location + 1, machine);
    		}
    	}
    }
    
    /**
     * Removes a machine from the user's machineList. 
     * Do not charge the user for the price of this machine
     * @param machineName the name of the machine to remove
     * @return the machine on success, null if no such machine found
     * @throws IllegalArgumentException if arguments are null
     */
    public Machine removeFromMachineList(String machineName) {
    	// null check
    	if (machineName == null) {
    		throw new IllegalArgumentException();
    	}
    	
    	// flag to keep track of any matches
    	boolean matchFound = false;
    	
    	// numeric value to track location of machine to remove
    	int removeIndex = -1;
    	for (int i = 0; i < machineList.size(); i++) {
    		// find match
    		if (machineName.equals(machineList.get(i).getName())) {
    			matchFound = true;
    			removeIndex = i;
    		}
    	}
    	//if no match, return null
    	if (!matchFound) {
    		return null;
    	}
    	//if match, return removed machine and remove it from list
    	Machine removedMachine = machineList.get(removeIndex);
    	machineList.remove(removeIndex);
    	return removedMachine;
    }
    
    /**
     * Print each machine in the user's machineList in its own line by using
     * the machine's toString function.
     */
    public void printMachineList() {
    	for (int i = 0; i < machineList.size(); i++) {
    		System.out.println(machineList.get(i).toString());
    	}
    }
    
    /**
     * Rents the specified machine in the user's machineList.
     * Charge the user according to the price of the machine by updating the 
     * credit.
     * Remove the machine from the machineList as well.
     * Throws an InsufficientCreditException if the price of the machine is 
     * greater than the credit available. Throw the message as:
     * Insufficient credit for <username>! Available credit is $<credit>.
     * 
     * @param machineName name of the machine
     * @return true if successfully bought, false if machine not found 
     * @throws InsufficientCreditException if price > credit 
     */
    public boolean rent (String machineName) throws InsufficientCreditException {
    	// variable to save matching machine
    	Machine rental = null;
    	for (int i = 0; i < machineList.size(); i++) {
    		// if machine name matches, save it as rental
    		if (machineName.equals(machineList.get(i).getName())) {
    			rental = machineList.get(i);
    		}
    	}

    	// numeric value to hold rental machine price
    	double machinePrice;
    	
    	// if there's a machine to rent, get its price
    	if (rental != null) {
    		machinePrice = rental.getPrice();
    	}
    	else { // if no machine to rent, return false
    		return false;
    	}
    	/* if rental machine's price is greater than 
    	 * credit, throw InsufficientCreditException
    	 */    	
    	if (machinePrice > credit) {
    		throw new InsufficientCreditException("Insufficient credit for " + 
    				username + "! Available credit is $" + credit + "."); 
    	}
    	/* if user has enough credit, deduct price of rental 
    	 * from their credit and remove it from their list
    	 */    	
    	else {
    		credit = credit - machinePrice;
    		removeFromMachineList(machineName);
    		System.out.println("Rented " + machineName);
    	}
    	return true;
    }
    
    /** 
     * Returns the credit of the user
     * @return the credit
     */
    public double getCredit() {
    	return credit;
    }

    /**
     * This method is already implemented for you. Do not change.
     * Declare the first n machines in the currentUser's machineList to be 
     * available.
     * n is generated randomly between 0 and size of the machineList.
     * 
     * @returns list of machines in stock 
     */
    public ListADT<Machine> generateMachineStock() {
        ListADT<Machine> availableMachines = new DLinkedList<Machine>();

        int size = machineList.size();
        if(size == 0)
            return availableMachines;
       
       //N items in stock where n >= 0 and n < size
        int n = randGen.nextInt(size+1); 

        //pick first n items from machineList
        for(int ndx = 0; ndx < n; ndx++)
            availableMachines.add(machineList.get(ndx));
        
        return availableMachines;
    }

}
