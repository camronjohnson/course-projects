// Main Class File:  Cloud.java
// File:             InsufficientCreditException.java
// Semester:         CS367 Summer 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
// Lecturer's Name:  Meena Syamkumar
//
// Pair Partner:     Azhar Siddiqui
// Email:            Siddiqui4@wisc.edu
// CS Login:         azhar
// Lecturer's Name:  Meena Syamkumar

/**
 * Stores the message of the exception
 * 
 * @author Camron Johnson
 */
public class InsufficientCreditException extends Exception{
	
	private String message;
	
	/**
     * Constructs an InsufficientCreditException. 
     */
	public InsufficientCreditException() {}
	
	/**
     * Constructs an InsufficientCreditException with a message. 
     * 
     * @param msg message about lack of credit
     */
	public InsufficientCreditException(String msg) {
		message = msg;
	}
	
	/**
     * Returns the message passed into the constructor.
     * 
     * @return message
     */
	public String getMessage() {
		return message;
	}
	
}
