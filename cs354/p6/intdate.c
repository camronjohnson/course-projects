////////////////////////////////////////////////////////////////////////////////
// Main File:        intdate.c
// This File:        division.c
// Other Files:      sendsig.c
// Semester:         CS 354 Fall 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

// variable to store time of system
time_t mytime;

// variable to track number of times SIGUSR1 is received
int sigReceived;

// handler for SIGALRM
void alm_handler(int sig) {
	mytime = time(NULL);
	printf("PID: %i | Current Time: %s", getpid(), ctime(&mytime));
	alarm(3);
}

// handler for SIGUSR1
void usr_handler(int sig) {
	sigReceived++;
	printf("SIGUSR1 caught!\n");
}

// handler for SIGINT
void int_handler(int sig) {
	printf("\nSIGINT received.\nSIGUSR1 was received %i times. Exiting now.\n", sigReceived);
	exit(0);
}

int main() {
	printf("Pid and time will be printed every 3 seconds.\n");
	printf("Enter ^C to end the program.\n");

	// initialize sigaction struct
	struct sigaction act;
	memset (&act, 0, sizeof(act)); 

	// assign handler to struct
	act.sa_handler = alm_handler;

	// declare handler
	if(sigaction(SIGALRM, &act, NULL) != 0) {
		printf("Error registering SIGALRM");
		exit(1);
	}

	// initialize sigaction struct
	struct sigaction act2;
	memset (&act2, 0, sizeof(act2));

	// assign handler to struct
	act2.sa_handler = usr_handler;

	// declare handler
	if(sigaction(SIGUSR1, &act2, NULL) != 0) {
		printf("Error registering SIGUSR1");
		exit(1);
	}

	// initialize sigaction struct
	struct sigaction act3;
	memset (&act3, 0, sizeof(act3));

	// assign handler to struct
	act3.sa_handler = int_handler;

	// declare handler
	if(sigaction(SIGINT, &act3, NULL) != 0) {
		printf("Error registering SIGINT");
		exit(1);
	}

	alarm(3);
	
	while(1);

	exit(0);
}
