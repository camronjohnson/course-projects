////////////////////////////////////////////////////////////////////////////////
// Main File:        intdate.c
// This File:        division.c
// Other Files:      sendsig.c
// Semester:         CS 354 Fall 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char *argv[]) {
	
	// make sure input format is correct
	if (argc != 3) {
		printf("Usage: <signal type> <pid>\n");
		exit(1);
	}

	// send input signal to intdate.c
	if(strcmp(argv[1], "-u") == 0) {
		kill(atoi(argv[2]), SIGUSR1);
	}
	else if (strcmp(argv[1], "-i") == 0) {
		kill(atoi(argv[2]), SIGINT);
	}

	exit(0);
}
