////////////////////////////////////////////////////////////////////////////////
// Main File:        intdate.c
// This File:        division.c
// Other Files:      sendsig.c
// Semester:         CS 354 Fall 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

// variable to count successful divison computations
int successOps;

// handler for SIGFPE
void fp_handler(int sig) {
	printf("Error: a division by 0 operation was attempted.\n");
	printf("Total number of operations completed successfully: %i\n", successOps);
	printf("The program will be terminated.\n");
	exit(0);
}

// handler for SIGINT
void int_handler(int sig) {
	printf("\nTotal number of operations successfully completed: %i\n", successOps);
	printf("The program will be terminated.\n");
	exit(0);
}

int main() {
	
	// initialize sigaction struct
	struct sigaction act;
	memset(&act, 0, sizeof(act));

	// assign handler to struct
	act.sa_handler = fp_handler;

	// declare handler
	if (sigaction(SIGFPE, &act, NULL) != 0) {
		printf("Error registering SIGFPE");
		exit(1);
	}

	// initialize sigaction struct
	struct sigaction act2;
	memset (&act2, 0, sizeof(act2));

	// assign handler to struct
	act2.sa_handler = int_handler;

	// declare handler
	if (sigaction(SIGINT, &act2, NULL) != 0) {
		printf("Error registering SIGINT");
		exit(1);
	}
	
	while(1) {
		printf("Enter first integer: ");
		char firstInt[100];
		fgets(firstInt, 100, stdin);
		int int1 = atoi(firstInt);

		printf("Enter second integer: ");
		char secondInt[100];
		fgets(secondInt, 100, stdin);
		int int2 = atoi(secondInt);
		
		printf("%i / %i is %i with a remainder of %i\n", int1, int2, int1/int2, int1%int2);
		successOps++;
	}
	
	exit(0);
}
