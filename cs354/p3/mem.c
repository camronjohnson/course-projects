////////////////////////////////////////////////////////////////////////////////
// Main File:        mem.c
// This File:        mem.c
// Other Files:      Makefile, mem.h
// Semester:         CS 354 Fall 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include "mem.h"

/*
 * This structure serves as the header for each allocated and free block
 * It also serves as the footer for each free block
 * The blocks are ordered in the increasing order of addresses 
 */
typedef struct blk_hdr {   	    

    int size_status;
  
    /*
    * Size of the block is always a multiple of 8
    * => last two bits are always zero - can be used to store other information
    *
    * LSB -> Least Significant Bit (Last Bit)
    * SLB -> Second Last Bit 
    * LSB = 0 => free block
    * LSB = 1 => allocated/busy block
    * SLB = 0 => previous block is free
    * SLB = 1 => previous block is allocated/busy
    * 
    * When used as the footer the last two bits should be zero
    */

    /*
    * Examples:
    * 
    * For a busy block with a payload of 20 bytes (i.e. 20 bytes data + an additional 4 bytes for header)
    * Header:
    * If the previous block is allocated, size_status should be set to 27
    * If the previous block is free, size_status should be set to 25
    * 
    * For a free block of size 24 bytes (including 4 bytes for header + 4 bytes for footer)
    * Header:
    * If the previous block is allocated, size_status should be set to 26
    * If the previous block is free, size_status should be set to 24
    * Footer:
    * size_status should be 24
    * 
    */

} blk_hdr;

/* Global variable - This will always point to the first block
 * i.e. the block with the lowest address */
blk_hdr *first_block = NULL;

// Global variable - Total available memory 
int total_mem_size = 0;

/* 
 * Function for allocating 'size' bytes
 * Returns address of allocated block on success 
 * Returns NULL on failure 
 * Here is what this function should accomplish 
 * - Check for sanity of size - Return NULL when appropriate 
 * - Round up size to a multiple of 8 
 * - Traverse the list of blocks and allocate the best free block which can accommodate the requested size 
 * - Also, when allocating a block - split it into two blocks
 * Tips: Be careful with pointer arithmetic 
 */
void* Mem_Alloc(int size) {  

	int val = total_mem_size - 12;


	if (val % 8 != 0) { // make size multiple of 8
		val -= (8 - val % 8);
	}
	if (size < 0 || size > val) { // check if size is in range
		return NULL;
	}

	size += 4; // add size of header

	if (size % 8 != 0) { // check if size requested plus header is multiple of 8
		size += (8 - size % 8);
	}

	int size_request = size;

	blk_hdr *current_block = first_block;
	int block_size = 0;
	int prev_alloc = 0;
	int is_alloc = 0;

	blk_hdr *best_fit_ptr = NULL;
	int best_fit = total_mem_size * 2;

	while (current_block != first_block + total_mem_size / 4) { // finding best fit

		block_size = current_block->size_status;
		prev_alloc = block_size & 2; // extracts second last bit
		is_alloc = block_size & 1; // extracts last bit
		block_size = block_size - prev_alloc - is_alloc; // actual block size
	

		if (is_alloc == 0) {  // if current block not allocated
			if (block_size > size_request && block_size < best_fit) { // if it is best fit, save it
				best_fit_ptr = current_block;
				best_fit = block_size;
			}
		}
	    current_block = (blk_hdr *)((char *)current_block + block_size); // move to next block
	}

	if (best_fit_ptr == NULL) { // return null if no block is big enough for request
		return NULL;
	}

	// split block to prevent fragmentation
	if ((best_fit_ptr->size_status - (best_fit_ptr->size_status & 3)) != size_request) {
		int offset = best_fit_ptr->size_status - size_request;
		if (offset < 0) {
			return NULL;
		}

		int data = offset & 3;
		int real_size = offset - data;

		blk_hdr *new_hdr = (blk_hdr *)((char *)best_fit_ptr + size_request); // set new block's header
		new_hdr->size_status = offset; // set size of new block
			
		// create footer for new block after splitting
		blk_hdr *footer = (blk_hdr *)((char *)new_hdr + (real_size - 4));

		// assign footer size_status to be true size
		footer->size_status = new_hdr->size_status - (new_hdr->size_status & 3);
	}
	else { // go change next block p bit since we are not adding header with that info
		blk_hdr *next_blk = (blk_hdr *)((char *)best_fit_ptr + (best_fit_ptr->size_status - 
			(best_fit_ptr->size_status & 3)));
		next_blk->size_status += 2; // tell's next block that this block is now allocated
	}

	// allocate block
	best_fit_ptr->size_status = size_request + 1 + prev_alloc;	

    return best_fit_ptr + 1;
}

/* 
 * Function for freeing up a previously allocated block 
 * Argument - ptr: Address of the block to be freed up 
 * Returns 0 on success 
 * Returns -1 on failure 
 * Here is what this function should accomplish 
 * - Return -1 if ptr is NULL or not within the range of memory allocated by Mem_Init()
 * - Return -1 if ptr is not 8 byte aligned or if the block is already freed
 * - Mark the block as free 
 * - Coalesce if one or both of the immediate neighbours are free 
 */
int Mem_Free(void *ptr) { 
    blk_hdr *blk_ptr = (blk_hdr *)ptr;

	if (blk_ptr == NULL || blk_ptr < first_block || 
			blk_ptr > (blk_hdr *)((char *)first_block + (total_mem_size -12))) {
		return -1;
	}
	if ((((intptr_t)ptr & 1) % 8) != 0) { // check ptr alignment is 8 byte aligned
		return -1;
	}

	// move pointer from payload to header
	blk_hdr *ptr_hdr = --blk_ptr;

	if ((ptr_hdr->size_status & 1) == 0) { // checks if block is already free
		return -1;
	}

	// Freeing block
	ptr_hdr->size_status -= 1;
	int ptr_real_size = ptr_hdr->size_status - (ptr_hdr->size_status & 3); // real size
	blk_hdr *next_blk_hdr = (blk_hdr *)((char *)ptr_hdr + ptr_real_size); // location of next header
	next_blk_hdr->size_status -= 2; // let next block know this block is now free
	blk_hdr *prev_blk_ftr = NULL;
	blk_hdr *prev_blk_hdr = NULL;
	blk_hdr *final_blk_hdr = NULL;

	// coalesce with previous block if it is free
	if ((ptr_hdr->size_status & 2) == 0) {
		prev_blk_ftr = ptr_hdr - 1; // moves 4 bytes back to previous block's footer
	
		prev_blk_hdr = (blk_hdr *)((char *)prev_blk_ftr - prev_blk_ftr->size_status + 4);

		// update previous block's size
		prev_blk_hdr->size_status += (ptr_hdr->size_status - (ptr_hdr->size_status & 3));

		// set final_blk_hdr and size status
		final_blk_hdr = prev_blk_hdr;
		final_blk_hdr->size_status = prev_blk_hdr->size_status;
	}
	else {
		// if prev block is not free, current block's header is final header of new block
		final_blk_hdr = ptr_hdr;
		final_blk_hdr->size_status = ptr_hdr->size_status;
	}

	int next_blk_real_size = next_blk_hdr->size_status - (next_blk_hdr->size_status & 3);
	
	blk_hdr *final_blk_ftr = NULL;

	// coalesce with next block if it is free
	if ((next_blk_hdr->size_status & 1) == 0) {

		// update final_blk_hdr->size_status
		final_blk_hdr->size_status += next_blk_real_size;

		// initialize final_blk_ftr
		final_blk_ftr = (blk_hdr *)((char *)next_blk_hdr + next_blk_real_size - 4);
		final_blk_ftr->size_status = final_blk_hdr->size_status - (final_blk_hdr->size_status & 3);
	}
	else {
		// add final_blk_ftr to current block if next block is allocated
		final_blk_ftr = (blk_hdr *)((char *)ptr_hdr + ptr_real_size - 4);
		final_blk_ftr->size_status = final_blk_hdr->size_status - (final_blk_hdr->size_status & 3);
	}

	return 0;
}

/*
 * For testing purpose
 * To verify whether a block is double word aligned
 */
void *start_pointer;

/*
 * Function used to initialize the memory allocator
 * Not intended to be called more than once by a program
 * Argument - sizeOfRegion: Specifies the size of the chunk which needs to be allocated
 * Returns 0 on success and -1 on failure 
 */
int Mem_Init(int sizeOfRegion) {   	   
    int pagesize;
    int padsize;
    int fd;
    int alloc_size;
    void* space_ptr;
    static int allocated_once = 0;
  
    if(0 != allocated_once) {
        fprintf(stderr,"Error:mem.c: Mem_Init has allocated space during a previous call\n");
        return -1;
    }
    if(sizeOfRegion <= 0) {
        fprintf(stderr,"Error:mem.c: Requested block size is not positive\n");
        return -1;
    }

    // Get the pagesize
    pagesize = getpagesize();

    // Calculate padsize as the padding required to round up sizeOfRegion to a multiple of pagesize
    padsize = sizeOfRegion % pagesize;
    padsize = (pagesize - padsize) % pagesize;

    alloc_size = sizeOfRegion + padsize;

    // Using mmap to allocate memory
    fd = open("/dev/zero", O_RDWR);
    if(-1 == fd){
        fprintf(stderr,"Error:mem.c: Cannot open /dev/zero\n");
        return -1;
    }
    space_ptr = mmap(NULL, alloc_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (MAP_FAILED == space_ptr) {
        fprintf(stderr,"Error:mem.c: mmap cannot allocate space\n");
        allocated_once = 0;
        return -1;
    }
  
    allocated_once = 1;
 
    // for double word alignement
    alloc_size -= 4;
 
    // Intialising total available memory size
    total_mem_size = alloc_size;

    // To begin with there is only one big free block
    // initialize heap so that first block meets double word alignement requirement
    first_block = (blk_hdr*) space_ptr + 1;
    start_pointer = space_ptr;
  
    // Setting up the header
    first_block->size_status = alloc_size;

    // Marking the previous block as busy
    first_block->size_status += 2;

    // Setting up the footer
    blk_hdr *footer = (blk_hdr*) ((char*)first_block + alloc_size - 4);
    footer->size_status = alloc_size;
  
    return 0;
}

/* 
 * Function to be used for debugging 
 * Prints out a list of all the blocks along with the following information for each block 
 * No.      : serial number of the block 
 * Status   : free/busy 
 * Prev     : status of previous block free/busy
 * t_Begin  : address of the first byte in the block (this is where the header starts) 
 * t_End    : address of the last byte in the block 
 * t_Size   : size of the block (as stored in the block header)(including the header/footer)
 */ 
void Mem_Dump() {   	   
    int counter;
    char status[5];
    char p_status[5];
    char *t_begin = NULL;
    char *t_end = NULL;
    int t_size;

    blk_hdr *current = first_block;
    counter = 1;

    int busy_size = 0;
    int free_size = 0;
    int is_busy = -1;

    fprintf(stdout,"************************************Block list***********************************\n");
    fprintf(stdout,"No.\tStatus\tPrev\tt_Begin\t\tt_End\t\tt_Size\n");
    fprintf(stdout,"---------------------------------------------------------------------------------\n");
  
    while (current < (blk_hdr*) ((char*)first_block + total_mem_size)) {
        t_begin = (char*)current;
        t_size = current->size_status;
    
        if (t_size & 1) {
            // LSB = 1 => busy block
            strcpy(status,"Busy");
            is_busy = 1;
            t_size = t_size - 1;
        }
        else {
            strcpy(status,"Free");
            is_busy = 0;
        }

        if (t_size & 2) {
            strcpy(p_status,"Busy");
            t_size = t_size - 2;
        }
        else 
            strcpy(p_status,"Free");

        if (is_busy) 
            busy_size += t_size;
        else 
            free_size += t_size;

        t_end = t_begin + t_size - 1;
    
        fprintf(stdout,"%d\t%s\t%s\t0x%08lx\t0x%08lx\t%d\n", counter, status, p_status, 
                    (unsigned long int)t_begin, (unsigned long int)t_end, t_size);
    
        current = (blk_hdr*)((char*)current + t_size);
        counter = counter + 1;
    }

    fprintf(stdout,"---------------------------------------------------------------------------------\n");
    fprintf(stdout,"*********************************************************************************\n");
    fprintf(stdout,"Total busy size = %d\n", busy_size);
    fprintf(stdout,"Total free size = %d\n", free_size);
    fprintf(stdout,"Total size = %d\n", busy_size + free_size);
    fprintf(stdout,"*********************************************************************************\n");
    fflush(stdout);
    return;
}
