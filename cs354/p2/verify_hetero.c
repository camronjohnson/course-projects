////////////////////////////////////////////////////////////////////////////////
// Main File:        verify_hetero.c
// This File:        verify_hetero.c
// Other Files:      none
// Semester:         CS 354 Fall 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Structure representing Square
// size: dimension(number of rows/columns) of the square
// array: 2D array of integers
typedef struct _Square {
	int size;
	int **array;
} Square;

Square construct_square(char *filename);
void insertion_sort(int* arr, int size);
int verify_hetero(Square * square);

int main(int argc, char *argv[])
{
	Square square;
	int valid;

	// Check input arguments to get filename
	if (argc != 2) {
		printf("Usage: ./generate_magic <filename>\n");
		exit(1);
	}
	
	// Construct square
	square = construct_square(argv[1]);
	
	// Verify if it's a heterosquare and print true or false
	valid = verify_hetero(&square);

	if (valid) {
		printf("true\n");
	}
	else {
		printf("false\n");
	}
	return 0;
}

/* construct_square reads the input file to initialize a square struct
 * from the contents of the file and returns the square.
 * The format of the file is defined in the assignment specifications
 */
Square construct_square(char *filename)
{
	FILE *ifp;
	char str[10000];
	char *str2;
	int num;
	char *token;

	// Open and read the file
	ifp = fopen(filename, "r");

	if (ifp == NULL) {
		printf("Cannot open file for reading.\n");
		exit(1);
	}
	
	// Read the first line to get the square size
	str2 = fgets(str, 10000, ifp);

	if (str2 == NULL) {
		printf("Cannot read file.\n");
		exit(1);
	}

	token = strtok(str, "\n");

	if (token == NULL) {
		printf("Cannot parse file data.\n");
		exit(1);
	}

	int squareSize = atoi(token);

	if (squareSize == 0) {
		printf("Error converting square size to int or invalid size.\n");
		exit(1);
	}

	// Initialize a new Square struct of that size
	Square *square;
	square = malloc(sizeof(Square));

	if (square == NULL) {
		printf("Error allocating memory.\n");
		exit(1);
	}

	square->size = squareSize;
	square->array = malloc(squareSize * sizeof(int*));

	if (square->array == NULL) {
		printf("Error allocating memory.\n");
		exit(1);
	}

	for (int i = 0; i < squareSize; ++i) {
		*(square->array + i) = malloc(squareSize * sizeof(int));
		if (*(square->array + i) == NULL) {
			printf("Error allocating memory.\n");
			exit(1);
		}
	}

	// Read the rest of the file to fill up the square
	int row = 0;
	
	while (fgets(str, 10000, ifp) != NULL) {
		token = strtok(str, ",");
		if (token == NULL) {
			printf("Cannot parse file data.\n");
			exit(1);
		}
		int col = 0;
		
		// fills array
		while (token != NULL) {
			num = atoi(token);
			*(*(square->array + row) + col) = num;
			token = strtok(NULL, ",");
			col++;
		}
		row++;
	}

	int check = fclose(ifp);

	if (check == EOF) {
		printf("Error closing file.\n");
		exit(1);
	}

	return *square;
}

/* insertion_sort sorts the arr in ascending order
 *
 */
void insertion_sort(int* arr, int size)
{
    // Sort the arr
	int j;
	int val;
	
	for (int i = 1; i < size; i++) {
		val = arr[i];
		j = i-1;
		
		while (j >= 0 && arr[j] > val) {
			arr[j+1] = arr[j];
			j = j-1;
		}
		arr[j+1] = val;
	}
}

/* verify_hetero verifies if the square is a heterosquare
 * 
 * returns 1(true) or 0(false)
 */
int verify_hetero(Square * square)
{
	// Calculate sum of the following and store it in an array
	// all rows and cols
	// main diagonal
	// secondary diagonal
	int arrayLength = (square->size * 2) + 2;
	int *array = malloc((arrayLength) * sizeof(int));

	if (array == NULL) {
		printf("Error allocating memory.\n");
		exit(1);
	}

	int sum;
	// sum all rows and store in first elements of array
	for (int j = 0; j < square->size; ++j) {
		sum = 0;
		for (int k = 0; k < square->size; ++k) {
			sum = sum + *(*(square->array + j) + k);
		}
		array[j] = sum;
	}

	// sum all cols and store in middle elements of array
	for (int j = 0; j < square->size; ++j) {
		sum = 0;
		for (int k = 0; k < square->size; ++k) {
			sum = sum + *(*(square->array + k) + j);
		}
		array[j + square->size] = sum;
	}

	// sum left-to-right diagonal
	sum = 0;
	for (int i = 0; i < square->size; ++i) {
		sum = sum + *(*(square->array + i) + i);
	}
	array[2 * square->size] = sum;

	// sum right-to-left diagonal
	sum = 0;
	for (int i = square->size - 1; i >= 0 ; i--) {
		sum = sum + *(*(square->array + square->size - i - 1) + i);
	}
	array[2 * square->size + 1] = sum;
       
	// Pass the array to insertion_sort func	
	insertion_sort(array, arrayLength);

    // Check the sorted array for duplicates
	int hetero = 1;

	for (int i = 0; i < arrayLength; ++i) {
		for (int j = i + 1; j < arrayLength; ++j)
			if (array[i] == array[j]) {
				hetero = 0;
				break;
			}
		if (!hetero) break;
	}

	// free 1D array
	free(array);
	array = NULL;

	// free 2D array
	for (int i = 0; i < square->size; ++i) {
		free(*(square->array + i));
	}
	free(square->array);
	square->array = NULL;

	if (hetero) {
		return 1;
	}
	return 0;
}
