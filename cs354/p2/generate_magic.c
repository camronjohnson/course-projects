////////////////////////////////////////////////////////////////////////////////
// Main File:        generate_magic.c
// This File:        generate_magic.c
// Other Files:      none
// Semester:         CS 354 Fall 2017
//
// Author:           Camron Johnson
// Email:            camron.johnson@wisc.edu
// CS Login:         camron
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// Structure representing Square
// size: dimension(number of rows/columns) of the square
// array: 2D array of integers
typedef struct _Square {
	int size;
	int **array;
} Square;

int get_square_size();
Square * generate_magic(int size);
void write_to_file(Square * square, char *filename);

int main(int argc, char *argv[])
{
	int squareSize;
	Square *square;

	// Check input arguments to get filename
	if (argc != 2) {
		printf("Usage: ./generate_magic <filename>\n");
		exit(1);
	}

	// Get size from user
	squareSize = get_square_size();
	
	// Generate the magic square
	square = generate_magic(squareSize);

	// Write the square to the output file
	write_to_file(square, argv[1]);

	// free 2D array
	for (int i = 0; i < square->size; ++i) {
		free(*(square->array + i));
	}
	free(square->array);
	square->array = NULL;
	
	return 0;
}

/* get_square_size prompts the user for the magic square size
 * checks if it is an odd number >= 3 and returns the number
 */
int get_square_size()
{
	int x;

	printf("Enter size of magic square, must be odd\n");
	scanf("%d", &x);

	if (x < 3 || x % 2 == 0) {
		printf("Size must be an odd number >= 3.\n");
		exit(1);
	}

	return x;
}

/* generate_magic constructs a magic square of size n
 * using the Siamese algorithm and returns the Square struct
 */
Square * generate_magic(int n)
{
	Square *square;
	square = malloc(sizeof(Square));


	if (square == NULL) {
		printf("Error allocating memory.\n");
		exit(1);
	}
	
	square->size = n;

	// allocate memory for square's array
	square->array = malloc(n * sizeof(int*));
	if (square->array == NULL) {
		printf("Error allocating memory.\n");
		exit(1);
	}

	for (int i = 0; i < n; ++i) {
		*(square->array + i) = malloc(n * sizeof(int));
		if (*(square->array + i) == NULL) {
			printf("Error allocating memory.\n");
			exit(1);
		}
	}

	// fill array with zeros which helps check if element is already filled
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			*(*(square->array + i) + j) = 0;
		}
	}

	// calculates middle row, last column element
	int val = 1;
	int curRow = n/2;
	int curCol = n - 1;

	*(*(square->array + curRow) + curCol) = val; // assign first square

	int prevRow;
	int prevCol;

	while (val < square->size * square->size) {
		++val;
		++curRow;
		++curCol;

		// wrap row if it is out of bounds
		if (curRow >= n) {
			curRow = 0;
		}

		// wrap col if it is out of bounds
		if (curCol >= n) {
			curCol = 0;
		}

		// if element has not been filled, fill it with val
		if (*(*(square->array + curRow) + curCol) == 0) {
			*(*(square->array + curRow) + curCol) = val;
		}
		
		// else, go to previous position and fill element to the left
		else {
			curRow = prevRow;
			curCol = prevCol - 1;
			*(*(square->array + curRow) + curCol) = val;
		}
		prevRow = curRow;
		prevCol = curCol;
	}
	
	return square;
}

/* write_to_file opens up a new file(or overwrites the existing file)
 * and writes out the square in the format expected by verify_hetero.c
 */
void write_to_file(Square * square, char *filename)
{
	FILE *fp;

	fp = fopen(filename, "w+");

	if (fp == NULL) {
		printf("Error opening file.\n");
		exit(1);
	}

	// write square size to file
	int val = fprintf(fp, "%i\n", square->size);

	if (val < 0) {
		printf("Error writing to file.\n");
		exit(1);
	}

	// write array to file
	for (int i = 0; i < square->size; ++i) {
		for (int j = 0; j < square->size; ++j) {
			val = fprintf(fp, "%i", *(*(square->array + i) + j));
			if (val < 0) {
				printf("Error writing to file.\n");
				exit(1);
			}
			if (j != square->size - 1) {
				fprintf(fp, ",");
			}
		}
		val = fprintf(fp, "\n");
		if (val < 0) {
			printf("Error writing to file.\n");
			exit(1);
		}
	}
	val = fclose(fp);

	if (val == EOF) {
		printf("Error closing file.\n");
		exit(1);
	}

}
