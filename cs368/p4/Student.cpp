////////////////////////////////////////////////////////////////////////////////
// File Name:      Student.cpp
//
// Author:         Camron Johnson
// CS email:       camron.johnson@wisc.edu
//
// Description:    This file provides basic functions for Student objects.
////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <iostream>

#include "Student.hpp"

std::vector<double> vec = {1.0};
std::vector<double>& assignmentsScore = vec;
int Student::numStudents = 0;

Student::Student(std::string name, std::vector<double> &assignmentsScore, double projectScore) {
	// initialize variables
	this->name = name;
	this->assignmentsScore = assignmentsScore;
	this->projectScore = projectScore;

	// count students
	id = numStudents++;
}

int Student::getNumStudents() {
	return numStudents;
}

int Student::getId() {
	return id;
}

std::string Student::getName() {
	return name;
}

std::vector<double>& Student::getAssignmentsScore() {
	return assignmentsScore;
}

double Student::getProjectScore() {
	return projectScore;
}

void Student::printDetails() {
	std::cout << "STUDENT DETAILS:" << std::endl << "Id = " << getId() << std::endl;
	std::cout << "Name = " << getName() << std::endl << "Assignments = [";
	
	std::vector<double> scores = getAssignmentsScore();

	// print scores
	for (int i = 0; i < scores.size(); i ++) {
		std::cout << scores[i];
		if (i != (scores.size() - 1)) {
			std::cout << ", ";
		}
	}
	std::cout << "]" << std::endl;
	
	std::cout << "Project = " << getProjectScore() << std::endl;
	std::cout << "Total = " << getTotal() << std::endl << "Grade = " << getGrade() << std::endl;
}
