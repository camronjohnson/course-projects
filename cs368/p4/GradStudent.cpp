////////////////////////////////////////////////////////////////////////////////
// File Name:      GradStudent.cpp
//
// Author:         Camron Johnson
// CS email:       camron.johnson@wisc.edu
//
// Description:    This file provides basic functions for GradStudent objects.
////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <iostream>

#include "GradStudent.hpp"

GradStudent::GradStudent(std::string name, std::vector<double>& assignmentsScore, 
	double projectScore, std::string researchArea, std::string advisor)
	: Student(name, assignmentsScore, projectScore) {
	// initialize variables
	this->researchArea = researchArea;
	this->advisor = advisor;
}

std::string GradStudent::getResearchArea() {
	return researchArea;
}

std::string GradStudent::getAdvisor() {
	return advisor;
}

void GradStudent::printDetails() {
	Student::printDetails();
	std::cout << "Type = Graduate Student" << std::endl << "Research Area = ";
	std::cout << getResearchArea();
	std::cout << std::endl << "Advisor = " << getAdvisor() << std::endl;
}

double GradStudent::getTotal() {
	double assignmentScore = 0;

	std::vector<double> scores = getAssignmentsScore();

	// sum assignment scores
	for (int i = 0; i < scores.size(); i++) {
		assignmentScore += scores[i];
	}

	// calculate total
	double total = (assignmentScore / (scores.size())) * 0.5 + 
		getProjectScore() * 0.5;

	return total;
}

std::string GradStudent::getGrade() {
	double score = getTotal();
	if (score >= 80) {
		return "CR";
	}
	else {
		return "N";
	}
}


