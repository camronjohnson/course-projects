////////////////////////////////////////////////////////////////////////////////
// File Name:      UndergradStudent.cpp
//
// Author:         Camron Johnson
// CS email:       camron.johnson@wisc.edu
//
// Description:    This file has basic functions for UndergradStudent objects.
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <iostream>

#include "UndergradStudent.hpp"

UndergradStudent::UndergradStudent(std::string name, std::vector<double> &assignmentsScore, 
	double projectScore, std::string residenceHall, std::string yearInCollege)
	: Student(name, assignmentsScore, projectScore) {
		// initialize variables
		this->residenceHall = residenceHall;
		this->yearInCollege = yearInCollege;
}

std::string UndergradStudent::getResidenceHall() {
	return residenceHall;
}

std::string UndergradStudent::getYearInCollege() {
	return yearInCollege;
}

void UndergradStudent::printDetails() {
	Student::printDetails();
	std::cout << "Type = Undergraduate Student" << std::endl << "Residence Hall = ";
	std::cout << getResidenceHall() << std::endl << "Year in College = ";
	std::cout << getYearInCollege() << std::endl;
}

double UndergradStudent::getTotal() {
	double assignmentScore = 0;

	std::vector<double> scores = Student::getAssignmentsScore();

	// sum assignment scores
	for (int i = 0; i < scores.size(); i ++) {
		assignmentScore += scores[i];
	}
	
	int projectScore = getProjectScore();

	// calculate total
	double total = (assignmentScore / (scores.size())) * 0.7 + getProjectScore() * 0.3;
	
	return total;
}

std::string UndergradStudent::getGrade() {
	double score = getTotal();
	if (score >= 70) {
		return "CR";
	}
	else {
		return "N";
	}
}

