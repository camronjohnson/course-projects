///////////////////////////////////////////////////////////////////////////////
// File Name:      processStudent.cpp
//
// Author:         Camron Johnson
// CS email:       camron.johnson@wisc.edu
//
// Description:    Methods to perform some processing on student objects.
//
// URL(s) of sources:
//                 https://stackexchange.com
///////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <iostream>
#include <istream>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <algorithm>

#include "Student.hpp"
#include "UndergradStudent.hpp"
#include "GradStudent.hpp"
#include "processStudent.hpp"

void fillStudents(std::istream &inFile,
                  std::vector<std::shared_ptr<Student>>& gstudentPtrs,
                  std::vector<std::shared_ptr<Student>>& ugstudentPtrs) {

	while (true) {
    	std::string studentInfo;

		std::getline(inFile, studentInfo); // read line

		if (inFile.fail()) {
			break;
		}	
	
		std::istringstream iss(studentInfo);

		std::string type;
		if (std::getline(iss, type, ',')) { // parse line
			if (type == "U") { // check if student is undergrad
				std::string name;
				getline(iss, name, ',');
				std::vector<double> assignmentScores;
				std::string scoreString;
				double scoreDouble;

				// add assignments to vector
				for (int i = 0; i < 6; i++) {
					getline(iss, scoreString, ',');
					scoreDouble = atof(scoreString.c_str());
					assignmentScores.push_back(scoreDouble);
				}
				std::string projectScoreString;
				getline(iss, projectScoreString, ',');
				double projectScore = atof(projectScoreString.c_str());
				std::string residenceHall;
				getline(iss, residenceHall, ',');
				std::string year;
				getline(iss, year, ',');

				// create UndergradStudent
				std::shared_ptr<Student> undergrad(new UndergradStudent(name, 
					assignmentScores, projectScore, residenceHall, year));
				ugstudentPtrs.push_back(undergrad); // add undergrad to vector
			}
			else if (type == "G") { // check if student is grad
				std::string name;
				getline(iss, name, ',');
				std::vector<double> assignmentScores;
				std::string scoreString;
				double scoreDouble;

				// add assignments to vector
				for (int i = 0; i < 6; i++) {
					getline(iss, scoreString, ',');
					scoreDouble = atof(scoreString.c_str());
					assignmentScores.push_back(scoreDouble);
				}
				std::string projectScoreString;
				getline(iss, projectScoreString, ',');
				double projectScore = atof(projectScoreString.c_str());
				std::string research;
				getline(iss, research, ',');
				std::string advisor;
				getline(iss, advisor, ',');

				// create GradStudent
				std::shared_ptr<Student> grad(new GradStudent(name, 
					assignmentScores, projectScore, research, advisor));
				gstudentPtrs.push_back(grad); // add grad to vector
			}
		}
	}
}

void printStudents(std::vector<std::shared_ptr<Student>>& students) {
	for (auto ptr : students) {
		ptr->printDetails();
		std::cout << std::endl;
	}
}

void computeStatistics(std::vector<std::shared_ptr<Student>>& students) {
    // display the number of students (undergrad or grad)
	std::cout << "Number of students = " << students.size() << std::endl;

    // compute the mean of the total score.
	std::cout << "The mean of the total score = ";
	
	double total;
	for (auto ptr : students) { // sum assignment scores
		total += ptr->getTotal();
	}

	double mean = total / students.size(); // calculate mean
	std::cout << mean << std::endl;

	std::cout << "The sorted list of students (id, name, total, grade) in ";
	std::cout << "descending order of total:" << std::endl;

    // sort and print the students based on their total.
	sort(students.begin(), students.end(), [] (std::shared_ptr<Student>& s1, 
		std::shared_ptr<Student>& s2) {return s1->getProjectScore() > s2->getProjectScore();});
	
	for (auto ptr : students) {
		std::cout << ptr->getId() << ", " << ptr->getName() << ", ";
		std::cout << ptr->getTotal() << ", " << ptr->getGrade() << std::endl;
	}

	std::cout << std::endl;
}
