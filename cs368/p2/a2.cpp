///////////////////////////////////////////////////////////////////////
// File Name:      a2.cpp
//
// Author:         Camron Johnson
// CS email:       camron@cs.wisc.edu
// 
// Description:   Reads two files containing an unknown number of
//                recipes and recipes found in both files and files
//                that are found in either file.
//
// URL(s) of sources:
//		https://stackoverflow.com/questions/19483663/vector-
//			intersection-in-c
//         
///////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

string firstFile, secondFile;
ifstream file1, file2;
ofstream outFile1, outFile2;
string recipe;
vector<string> recipeVec1;
vector<string> recipeVec2;


/**
* @brief Forms a sorted set of recipes that are in both files.
*
* @param param_1 Vector containing the recipes from the first file.
* 
* @param param_n Vector containing the recipes from the second file.
* 
* @return A sorted vector of recipes found in each file.
*/
vector<string> intersec(vector<string> &v1, vector<string> &v2) {
	vector<string> v3;
	sort(v1.begin(), v1.end()); // sorts the first vector
	sort(v2.begin(), v2.end()); // sorts the second vector
	set_intersection(v1.begin(),v1.end(),v2.begin(),v2.end(),back_inserter(v3));

	return v3;
}

/**
* @brief Forms a sorted set of recipes that are in either file.
*
* @param param_1 Vector containing the recipes from the first file.
* 
* @param param_n Vector containing the recipes from the second file.
* 
* @return A sorted vector of recipes found in either file.
*/
vector<string> formUnion(vector<string> &v1, vector<string> &v2) {
	
	vector<string> v3;
	sort(v1.begin(), v1.end()); // sorts the first vector
	sort(v2.begin(), v2.end()); // sorts the second vector
	set_union(v1.begin(),v1.end(),v2.begin(),v2.end(),back_inserter(v3));

	return v3;
}

/**
* @brief Gets the file names from the user.
*/
void getFiles() {
	bool found = false;
	while (!found) { // get first file
		cout << "Enter the name of the first file: ";
		cin >> firstFile;

		file1.open(firstFile);
		if(!file1.is_open()) { // if it isn't open, show error message
			cout << "Input file " << firstFile << " is NOT found. Please try again." << endl;
		}
		else {
			found = true;
		}
	}

	found = false;
	while (!found) { // get second file
		cout << "Enter the name of the second file: ";
		cin >> secondFile;

		file2.open(secondFile);
		if (!file2.is_open()) { // if it isn't open, show error message
			cout << "Input file " << secondFile << " is NOT found. Please try again." << endl;
		}
		else {
			found = true;
		}
	}
}

/**
* @brief Reads the files and processes the files.
*/
void readFiles() {
	while(getline(file1, recipe)) { // add each recipe to vector
		recipeVec1.push_back(recipe);
	}

	while(getline(file2, recipe)) { // add each recipe to vector
		recipeVec2.push_back(recipe);
	}
}

/**
* @brief Prints the number of recipes in each file, the number of recipes in both files,
*        and the number of recipes in either file. This method also write the recipes
*		 in both or either files to new files.
*/
void compareFiles() {

	cout << endl << "Number of recipes in " << firstFile << " = " << recipeVec1.size() << endl;
	cout << "Number of recipes in " << secondFile << " = " << recipeVec2.size() << endl;

	auto intersecVec = intersec(recipeVec1, recipeVec2);

	cout << "Number of recipes that are present in BOTH " << firstFile << " AND ";
	cout << secondFile << " = " << intersecVec.size() << endl;

	auto unionVec = formUnion(recipeVec1, recipeVec2);

	cout << "Number of recipes that are in EITHER " << firstFile << " OR ";
	cout << secondFile << " = " << unionVec.size() << endl << endl;

	// only print recipes in both files if there are any recipes that are in both
	if (intersecVec.size() != 0) {
		cout << "List of recipes that are present in BOTH " << firstFile << " AND ";
		cout << secondFile << ":" << endl;

		for (string n : intersecVec) {
			cout << n << endl;
		}
		cout << endl;
	}

	// write recipes in each file to new file
	outFile1.open("intersection.txt");
	for (string n : intersecVec) {
		outFile1 << n << endl;
	}

	// write recipes in either file to new file
	outFile2.open("union.txt");
	for (string n : unionVec) {
		outFile2 << n << endl;
	}

	cout << "The set intersection of the two files was written to a file named intersection.txt";
	cout << endl << "The set union of the two files was written to a file named union.txt" << endl;
}

/**
* @brief Calls methods to process files containing various recipes.
*
* @return returns 0 if there is no error.
*/
int main() {

	getFiles();
	readFiles();
	compareFiles();

	return 0;
}
