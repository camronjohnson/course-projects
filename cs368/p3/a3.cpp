////////////////////////////////////////////////////////////////////////////////
// File Name:      a3.cpp
//
// Author:         Gerald, Isaac, Varun, Camron Johnson
// CS email:       gerald@cs.wisc.edu
//                 cisung@wisc.edu
//                 vnaik@cs.wisc.edu
//                 camron.johnson@wisc.edu
//
// Description:    The source file for a3.
//
// IMPORTANT NOTE: THIS IS THE ONLY FILE THAT YOU SHOULD MODIFY FOR A3.
//                 You SHOULD NOT MODIFY any of the following:
//                   1. Name of the functions.
//                   2. The number and type of parameters of the functions.
//                   3. Return type of the functions.
//                   4. Import statements.
//
// URL(s) of sources:
//                 https://stackexchange.com
////////////////////////////////////////////////////////////////////////////////

#include "a3.hpp"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

void cleanData(std::istream &inFile, std::ostream &outFile,
               std::unordered_set<std::string> &commonWords) {
	string line;
	vector<string> words;

	// remove common words and write them to a file
	while (getline(inFile, line)) {
		
		// parse line
		splitLine(line, words);

		removeCommonWords(words, commonWords);

		// write each element in vector to file
		for (auto it = 	words.begin(); it != words.end(); ++it) {
			if (it != words.begin()) {
				outFile << ' ' << *it;
			}
			else {
				outFile << *it;
			}
		}
		outFile << endl;

		// clear vector before reading next line
		words.clear();
	}
}

void fillDictionary(std::istream &newInFile,
                    std::unordered_map<std::string, std::pair<long, long>> &dict) {
	int rating;
	string line;
	vector<string> words;
	unordered_map<string, pair<long, long>>::iterator mapIt;
	
	while (newInFile >> rating) {

		getline(newInFile, line);

		// parse line
		splitLine(line, words);

		// add words to map if not already in map or update the value for the word
		for (auto it = 	words.begin(); it != words.end(); ++it) {
			mapIt = dict.find(*it);
			if (mapIt == dict.end()) {
				dict.insert(make_pair(*it, make_pair(rating, 1)));	
			}
			else {
				mapIt->second.first += rating;
				++mapIt->second.second;
			}
		}

		// clear vector before reading next line
		words.clear();
	}
}

void fillCommonWords(std::istream &inFile,
                   std::unordered_set<std::string> &commonWords) {
	string word;

	// add common words to unordered_set
	while(getline(inFile, word)) {
			commonWords.insert(word);
	}
}

void rateReviews(std::istream &testFile,
                 std::unordered_map<std::string, std::pair<long, long>> &dict,
                 std::ostream &ratingsFile) {
	string line;
	vector<string> words;
	double numWords;
	double wordsRating;
	double reviewRating;
	unordered_map<string, pair<long, long>>::iterator mapIt;

	while (getline(testFile, line)) {
		if (line != "") {

			// parse line
			splitLine(line, words);
		
			numWords = 0;
			wordsRating = 0;

			/* if word is in map, update line's total rating by corresponding 
			 * rating, else increment wordsRating by 2
			 */
			for (auto it = 	words.begin(); it != words.end(); ++it) {
				++numWords;
				mapIt = dict.find(*it);
				if (mapIt == dict.end()) {
					wordsRating += 2;
			}
				else {
					wordsRating += (double)mapIt->second.first / mapIt->second.second;
				}
			}

			// calculate review's rating
			reviewRating = wordsRating / numWords;
		}
		else {
			// if line is empty, review is assigned rating of 2
			reviewRating = 2;
		}

		// write review's rating to file with 3 significant figures
		ratingsFile << setprecision(2) << fixed << reviewRating << endl;

		// clear vector before reading next line
		words.clear();
	}
}

void removeCommonWords(std::vector<std::string> &tokens,
                     std::unordered_set<std::string> &commonWords) {
	// if a commond word is found in the vector, remove it from the vector
	for (auto it = commonWords.begin(); it != commonWords.end(); ++it) {
		if (find(tokens.begin(), tokens.end(), *it) != tokens.end()) {
			tokens.erase(std::remove(tokens.begin(), tokens.end(), *it), tokens.end());
		}
	}
}

void splitLine(std::string &line, std::vector<std::string> &words) {
    // This code is provided for you and is correct.
    // You should NOT make any changes to this function!
    std::stringstream ss;
    ss.str(line);
    std::string token;
    while (std::getline(ss, token, ' ')) {
        if (token.length() != 0) {
            words.push_back(token);
        }
    }
}
