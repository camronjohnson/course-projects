#include <iostream>
#include "Vector.hpp"

void printVector(const Vector<int> &v) {
	if (v.size() == 0) {
		std::cout << "Array is Empty" << std::endl;
		return;
	}
	for (int i = 0; i < v.size(); ++i) {
		std::cout << v[i] << ", ";
	}
	std::cout << std::endl;
}

int main() {
	std::cout << "Resource Management!" << std::endl;
	Vector<int> v1;
	v1.pushBack(1);
	v1.pushBack(2);
	v1.pushBack(3);
	v1.pushBack(4);
	v1.pushBack(5);
	v1.reserve(10);
	printVector(v1);
	Vector<int> v2(v1); // Vector<int> v2 = v1;
	printVector(v2);
	v2[0] = 100;
	printVector(v2);
	printVector(v1);
	auto it = v2.begin();
	std::cout << "First element of v2 = " << *it << std::endl;
	std::cout << "Capacity of v1 is: " << v1.capacity() << ". Should be 10.";
	std::cout << std:: endl;
	std::cout << "First element of v2 = " << v2[0] << std::endl;
	std::cout << "v2.at(0) = " << v2.at(0) << std::endl;
	//v1.at(20);
	v1.clear();
	printVector(v2);
	v2.erase(v2.begin());
	printVector(v2);

	std::cout << "about to erase end" << std::endl;
	v2.erase(v2.end() - 1);
	std::cout << "just erased end" << std::endl;
	printVector(v2);

	return 0;
}
