#ifndef SMARTINTEGER_HPP
#define SMARTINTEGER_HPP

#include <iostream>
#include <limits.h>

class SmartInteger {

private:
	int num;

public:
	SmartInteger();
	SmartInteger(int val);
	int getValue() const;
	friend std::ostream& operator<<(std::ostream& os, const SmartInteger& rhs);
	bool operator<(const SmartInteger& rhs) const;
	bool operator>(const SmartInteger& rhs) const;
	bool operator<=(const SmartInteger& rhs) const;
	bool operator>=(const SmartInteger& rhs) const;
	bool operator==(const SmartInteger& rhs) const;
	bool operator!=(const SmartInteger& rhs) const;
	const SmartInteger operator+(const SmartInteger& rhs) const;
	SmartInteger operator-(const SmartInteger& rhs) const;
	SmartInteger operator*(const SmartInteger& rhs) const;
	SmartInteger& operator+=(const SmartInteger& rhs);
	SmartInteger& operator-=(const SmartInteger& rhs);
	SmartInteger& operator*=(const SmartInteger& rhs);
	SmartInteger& operator++();
	SmartInteger& operator--();
};

#endif
