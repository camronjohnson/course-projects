#include <iostream>
#include <limits.h>
#include "SmartInteger.hpp"

SmartInteger::SmartInteger() {
	num = 0;
}

SmartInteger::SmartInteger(int val) {
	num = val;
}

int SmartInteger::getValue() const {
	return num;
}

std::ostream& operator<<(std::ostream& os, const SmartInteger& rhs) {
	os << rhs.getValue();
	return os;
}

bool SmartInteger::operator<(const SmartInteger& rhs) const {
	return this->num < rhs.getValue();
}

bool SmartInteger::operator>(const SmartInteger& rhs) const {
	return rhs < *this;
}

bool SmartInteger::operator<=(const SmartInteger& rhs) const {
	return !(rhs < *this);
}

bool SmartInteger::operator>=(const SmartInteger& rhs) const {
	return !(*this < rhs);
}

bool SmartInteger::operator==(const SmartInteger& rhs) const {
	return !(*this < rhs || rhs < *this);
}

bool SmartInteger::operator!=(const SmartInteger& rhs) const {
	return (*this < rhs || rhs < *this);
}

const SmartInteger SmartInteger::operator+(const SmartInteger& rhs) const {
	
	if ((rhs.getValue() > 0) && (this->num > INT_MAX - rhs.getValue())) {
		throw std::exception();
	}

	if ((rhs.getValue() < 0) && (this->num < INT_MIN - rhs.getValue())) {
		throw std::exception();
	}

	SmartInteger sum = SmartInteger(this->num + rhs.getValue());
	return sum;
}

SmartInteger SmartInteger::operator-(const SmartInteger& rhs) const {
	
	if ((rhs.getValue() < 0) && (this->num > INT_MAX + rhs.getValue())) {
		throw std::exception();
	}

	if ((rhs.getValue() > 0) && (this->num < INT_MIN + rhs.getValue())) {
		throw std::exception();
	}

	SmartInteger dif = SmartInteger(this->num - rhs.getValue());
	return dif;
}

SmartInteger SmartInteger::operator*(const SmartInteger& rhs) const {

	if (this->num == 0 || rhs.getValue() == 0) {
		SmartInteger zero = SmartInteger();
		return zero;
	}

	if ((rhs.getValue() > 0) && (this->num > INT_MAX / rhs.getValue())) {
		throw std::exception();
	}

	if ((rhs.getValue() > 0) && (this->num < INT_MIN / rhs.getValue())) {
		throw std::exception();
	}
	
	
	if ((rhs.getValue() < 0) && (this->num < INT_MIN / ((-1) * rhs.getValue()))) {
		throw std::exception();
	}

	if ((rhs.getValue() < 0) && (this->num > INT_MAX / ((-1) * rhs.getValue()))) {
		throw std::exception();
	}
	

	SmartInteger product = SmartInteger(this->num * rhs.getValue());
	return product;
}

SmartInteger& SmartInteger::operator+=(const SmartInteger& rhs) {
	*this = *this + rhs;
	return *this;
}

SmartInteger& SmartInteger::operator-=(const SmartInteger& rhs) {
	*this = *this - rhs;
	return *this;
}

SmartInteger& SmartInteger::operator*=(const SmartInteger& rhs) {
	*this = *this * rhs;
	return *this;
}

SmartInteger& SmartInteger::operator++() {
	if (this->num == INT_MAX) {
		throw std::exception();
	}
	this->num += 1;
	return *this;
}

SmartInteger& SmartInteger::operator--() {
	if (this->num == INT_MIN) {
		throw std::exception();
	}
	this->num -= 1;
	return *this;
}
