#include "SmartInteger.hpp"

#include <iostream>

int main() {
    std::cout << "Constructing n1 and n2..." << std::endl;
    SmartInteger n1(3);
    SmartInteger n2;
    std::cout << "n1.getValue() should be 3: " << n1.getValue() << std::endl;
    std::cout << "n2.getValue() should be 0: " << n2.getValue() << std::endl;
    std::cout << std::endl;

    // No need to implement custom copy assignment operator - this should just work
    std::cout << "Changing value of n2..." << std::endl;
    n2 = 2;
    std::cout << "n2.getValue() should be 2: " << n2.getValue() << std::endl;
    std::cout << std::endl;

    // No need to implement custom copy constructor - this should just work
    std::cout << "Constructing n3..." << std::endl;
    SmartInteger n3(n2);
    std::cout << "n3.getValue() should be 2: " << n3.getValue() << std::endl;

	std::cout << std::endl;
	// testing '<<' operator
	std::cout << "Printing n3, should be 2: "<< n3 << std::endl;

	std::cout << std::endl;
	// testing '<' operator
	std::cout << "Comparing n1 < n2, should be 'n1 >= n2': ";

	if (n1 < n2) {
		std::cout << "n1 < n2" << std::endl;
	}
	else {
		std::cout << "n1 >= n2" << std::endl;
	}

	std::cout << std::endl;

	// testing '>' operator
	std::cout << "Comparing n1 > n2, should be 'n1 > n2': ";

    if (n1 > n2) {
    	std::cout << "n1 > n2" << std::endl;
	}
	else {
		std::cout << "n1 <= n2" << std::endl;
	}
	std::cout << std::endl;

	// testing '<=' operator
	std::cout << "Comparing n1 <= n2, should be 'n1 > n2': ";  

	if (n1 <= n2) {
		std::cout << "n1 <= n2" << std::endl;	
	}
	else {
		std::cout << "n1 > n2" << std::endl;
	}
	std::cout << std::endl;

	// testing '>=' operator
	std::cout << "Comparing n1 >= n2, should be 'n1 >= n2': ";	
		if (n1 >= n2) {
			std::cout << "n1 >= n2" << std::endl;
		}
		else {
			std::cout << "n1 < n2" << std::endl;
		}
	std::cout << std::endl;
	
	// testing '==' operator
	std::cout << "Comparing n1 == n2, should be 'n1 != n2': ";
		if (n1 == n2) {
			std::cout << "n1 == n2" << std::endl;
		}
		else {
			std::cout << "n1 != n2" << std::endl;
		}
	std::cout << std::endl;
	
	// testing '!=' operator
	std::cout << "Comparing n1 != n2, should be 'n1 != n2': ";
		if (n1 != n2) {
			std::cout << "n1 != n2" << std::endl;
		}
		else {
			std::cout << "n1 == n2" << std::endl;
		}

	std::cout << std::endl;

	// testing '+' operator
	std::cout << "Adding n1 and n2, should get: 5: ";
	std::cout << n1 + n2 << std::endl;

	std::cout << std::endl;

	// testing '-' operator
	std::cout << "Subtracting n2 from n1, should get 1: ";
	std::cout << n1 - n2 << std::endl;

	std::cout << std::endl;

	// testing '*' operator
	std::cout << "Multiplying n1 and n2, should get 6: ";
	std::cout << n1 * n2 << std::endl;

	std::cout << std::endl;

	// testing '+=' operator
	std::cout << "n1: " << n1 << " , n2: " << n2 << " , n3: " << n3 << std::endl;
	std::cout << "Doing n1 += n2, should get 5: ";
	n1 += n2;
	std::cout << n1.getValue() << std::endl;

	std::cout << "n1: " << n1 << " , n2: " << n2 << " , n3: " << n3 << std::endl;
	std::cout << "Doing (n1 += n2) += n3, should get 9: ";
	(n1 += n2) += n3;
	std::cout << n1.getValue() << std::endl;

	std::cout << std::endl;

	// testing '-=' operator
	std::cout << "n1: " << n1 << " , n2: " << n2 << " , n3: " << n3 << std::endl;
	std::cout << "Doing n1 -= n2, should get 7: ";
	std::cout << (n1 -= n2) << std::endl;

	std::cout << "n1: " << n1 << " , n2: " << n2 << " , n3: " << n3 << std::endl;
	std::cout << "Doing (n1 -= n2) -= n3, should get 3: ";
	(n1 -= n2) -= n3;
	std::cout << n1.getValue() << std::endl;

	std::cout << std::endl;

	// testing '*=' operator
	std::cout << "Doing n1 *= n2, should get 6: ";
	std::cout << (n1 *= n2) << std::endl;
	std::cout << std::endl;

	//testing '++' operator
	//std::cout << "Doing ++n1, should get 7: ";
	std::cout << ++n1 << std::endl;
	//std::cout << "n1 should be 7: " << n1 << std::endl;

	std::cout << std::endl;

	//testing '--' operator
	//std::cout << "Doing --n1, should get 6: ";
	std::cout << --n1 << std::endl;
	//std::cout << "n1 should be 6: " << n1 << std::endl;


		
	return 0;
}
